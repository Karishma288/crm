import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { HttpService } from '../http.service';
import { ActivatedRoute, RouteConfigLoadEnd, Router } from '@angular/router';
import { Observable, observable } from 'rxjs';
@Component({
  selector: 'app-staff',
  templateUrl: './staff.component.html',
  styleUrls: ['./staff.component.scss', '../admin/table.scss']
})
export class StaffComponent implements OnInit {
  staffForm = this.fb.group({
    name: [''],
    contact: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern('^((\\+91-?)|0)?[0-9]{10}$')]],
    email: [''],
    designation: [],
    password: [],
    count: ''


  });
  loader: boolean = true;
  error: string | undefined;
  staffData: any;
  params: any;
  showSiderBar: boolean = true;
  constructor(private fb: FormBuilder, private httpService: HttpService, private toasterService: ToastrService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: any) => {
      this.params = params
      console.log("this.params==>", this.params.id);

      if (params.id) {
        this.httpService.getStaff(params).subscribe(res => {
          const staff: any = res;
          console.log(staff);
          if (res) {
            this.staffForm.get('name')?.setValue(staff.Name)
            this.staffForm.get('email')?.setValue(staff.Email)
            this.staffForm.get('contact')?.setValue(staff.Contact)
            this.staffForm.get('designation')?.setValue(staff.Designation)
            this.staffForm.get('password')?.setValue(staff.PlainPass)
          }
        })
      }

    })
  }

  save() {
    this.loader = false;
    let data: any = {
      Name: this.staffForm.get('name')?.value,
      Contact: this.staffForm.get('contact')?.value,
      Email: this.staffForm.get('email')?.value,
      Designation: this.staffForm.get('designation')?.value,
      Password: this.staffForm.get('password')?.value,
      PlainPass: this.staffForm.get('password')?.value,
      // Role: ['sales'],
      // count: 0
    }

    if (this.params.id) {
      this.httpService.putupdateStaff(this.params.id, data).subscribe((res) => {
        this.loader = true;
        this.toasterService.success('Change Successfully')
        if (res == 'Successfull') {
          // this.toasterService.success('new staff update Successfully')
        }
      })
    }
    else {
      this.httpService.postnewstaff('SignUp', data).subscribe(res => {
        console.log(data)
        if (res == 'Successfull') {
          this.toasterService.success('new staff add Successfully');
          this.loader = true;
        }
        else if (res == 'email error') {
          console.log('errr', res);
          this.toasterService.error('Email already exist');
          this.loader = true;
        } else {
          this.toasterService.error('Contact already exist');
          this.loader = true;
        }
      }
      )
    }
  }
  openSiderBar() {
    console.log("inside open sidebar")
    this.showSiderBar = !this.showSiderBar
  }
  mobileNO(event: any) {
    this.httpService.mobileNumber(event);
  }

  // previousState() {
  //   window.history.back();
  // }

}