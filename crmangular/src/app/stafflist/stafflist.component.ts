import { Component, OnInit } from '@angular/core';
// import { Toast, ToastrService } from 'ngx-toastr';
import { ToastrService } from 'ngx-toastr';
import { CSVService } from '../csv.service';
import { HttpService } from '../http.service';


@Component({
  selector: 'app-stafflist',
  templateUrl: './stafflist.component.html',
  styleUrls: ['./stafflist.component.scss', '../admin/table.scss']
})

export class StafflistComponent implements OnInit {
  staffs: any;
  isExportingCSV = false;
  isImportingCSV1 = false;
  showSiderBar: boolean = true;
  activeField: any;
  loader: boolean = true;

  constructor(private csvService: CSVService, private httpService: HttpService,
    private toasterService: ToastrService,
  ) { }

  ngOnInit(): void {
    this.httpService.getstafflist('getUser').subscribe(res => {
      this.staffs = res;
      console.log("this.staffs====>", this.staffs);

      this.loader = false;
      // console.log(" this.staffs==========>", this.staffs);
    })

  }


  // exportLeadCSV() {
  //   this.isExportingCSV = true;
  //   const csvData = [];
  //   {
  //     const csvRow = {
  //       Name: '',
  //       Phone: '',
  //       Email: '',
  //       Location: '',
  //       ProjectDetails: '',
  //       status: '',

  //     };
  //     csvData.push(csvRow);
  //     this.csvService.downloadFile(csvData, 'SalesCsv', ['Name', 'Phone', 'Email', 'Location', 'ProjectDetails', 'Status']);
  //     this.isExportingCSV = false;
  //   }
  // }

  // onCSVSelected(event: any) {
  //   const files = event.target.files

  //   if (files && files.length > 0) {
  //     this.importCSV(files[0]);
  //   }
  // }
  // importCSV(csvFile: any) {
  //   this.isImportingCSV1 = true;
  //   this.csvService.csv2Array(csvFile).then((data: any[]) => {
  //     this.httpService.uploadCsvData(data).subscribe((res) => {
  //       if (res)
  //         this.toasterService.success('upload')

  //       this.isImportingCSV1 = false;

  //     })

  //   })

  // }

  openSiderBar() {
    console.log("inside open sidebar")
    this.showSiderBar = !this.showSiderBar
  }

  activeButton(i: any, status: any) {
    this.staffs[i].status = status;
    // const data = { id: this.staffs[i]._id }
    this.httpService.updateOnlyStatus(this.staffs[i]._id, this.staffs[i]).subscribe(res => {
      console.log(res)
      if (status == false) {
        this.toasterService.success("Inactive")
      }
      else {
        this.toasterService.success("Active")
      }
    })
  }
  revokeButton(i: any, revokeStatus: any) {
    this.staffs[i].revoke = revokeStatus;
    const data = { id: this.staffs[i]._id }
    this.httpService.updateOnlyStatus(this.staffs[i]._id, this.staffs[i]).subscribe(res => {
      console.log(res)
      if (revokeStatus == false) {
        this.toasterService.success("Lead Assignment No")
      }
      else {
        this.toasterService.success("Lead Assignment Yes")
      }
    })

  }


}
