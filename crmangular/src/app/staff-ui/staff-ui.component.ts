import { Component, OnInit, TemplateRef } from '@angular/core';
import { CSVService } from '../csv.service'
import { HttpService } from '../http.service'
// import { BsModalRe } from 'ngx-bootstrap-modal';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { IDropdownSettings } from "ng-multiselect-dropdown";
import { ActivatedRoute, RouteConfigLoadEnd, Router } from '@angular/router';
import { DatePipe, NgClass } from '@angular/common';

import * as moment from 'moment';
import { ThisReceiver } from '@angular/compiler';
import { validate } from 'uuid';
import { getPackageDate } from '../common';

@Component({
  selector: 'app-staff-ui',
  templateUrl: './staff-ui.component.html',
  styleUrls: ['./staff-ui.component.scss', '../admin/table.scss', '../sidebar/sidebar.component.scss']
})
export class StaffUiComponent implements OnInit {
  modalRef: any;
  modalRef1: any;
  modalRef2: any;
  status: any
  pageIndex: any;
  pageSize: any;
  length: any;
  statusButton: any;
  notShow: boolean = true;
  followUpButton: boolean = false;
  saleManageStatus = 'pending';
  currentPage = 0;
  totalRecords = 0;
  sizePerPage = 50;
  loader: boolean = true;
  showSiderBar: boolean = true;
  notFoundeImg: boolean = false;
  mobileBox: boolean = false;
  indexData: any;
  mobileInputArray = [];
  inputName: any;
  phnoneField: any;
  emailField: any;
  hideInput: any;
  leadNumData = new FormControl();
  phNumberArray: any = [];
  emailArray: any = [];
  allPhNumber: any = [];
  allEmaiArray: any = [];
  allGoogleDocArray: any = [];
  NotData: boolean = false;
  tech = [
    { id: 1, name: "Mobile", },
    { id: 2, name: "Web", },
    { id: 3, name: "Marketing", },
    { id: 4, name: "Testing", },
    { id: 5, name: "Design", },
    { id: 6, name: "Ecommerce", },
  ];

  mobileData = [
    { id: 1, name: "Flutter" },
    { id: 2, name: "ReactNative" },
    { id: 3, name: "Kotlin" },
    { id: 4, name: "Swift" },
    { id: 5, name: "Ruby" },
    { id: 6, name: "Rust" },
    { id: 7, name: "Lua" },
    { id: 8, name: "Dart" },
    { id: 9, name: "ActionScript" },
  ]
  TestingData = [
    { id: 1, name: "ManualTesting" },
    { id: 2, name: "AutomatedTesting" },
    { id: 3, name: "MobileDeviceTesting" },
    { id: 4, name: "FunctionalTesting" },
    { id: 5, name: "UsabilityTesting" },
    { id: 6, name: "CompatibilityTesting" },
    { id: 7, name: "PerformanceAndLoadTesting" },
    { id: 8, name: "SecurityTesting" },
  ];
  DesignData = [
    { id: 1, name: "AdobeXD" },
    { id: 2, name: "Photopea" },
    { id: 3, name: "Photoshop" },
    { id: 4, name: "Figma" },
  ]
  ecommerceData = [
    { id: 1, name: "FrontEnd" },
    { id: 2, name: "BackEnd" },
  ]
  webDatafrontEnd = [
    { id: 1, name: 'Angular' },
    { id: 2, name: 'React' },
    { id: 3, name: 'Vue' },
    { id: 4, name: 'Html' },
    { id: 5, name: 'Css' },
    { id: 6, name: 'Javascript' },
    { id: 7, name: 'Jquery' }
  ]
  webDataBackEnd = [
    { id: 1, name: 'Nodejs' },
    { id: 2, name: 'NestJs' },
    { id: 3, name: 'Python' },
    { id: 4, name: 'Php' },
    { id: 5, name: 'Java' },
    { id: 6, name: 'Perl' },
  ]
  marketing = [
    { id: 1, name: 'SEO' },
    { id: 2, name: 'SMO' },
    { id: 3, name: 'Content' },
    { id: 4, name: 'SMM' },
    { id: 5, name: 'GMB' },
    { id: 6, name: 'GoogleAbs' },
    { id: 7, name: 'GoogleAnalytics' },
    { id: 8, name: 'Others' },
  ]
  selectTech: any[] = []
  selectMobile: any[] = [];
  selectTesting: any = [];
  selectDesign: any = [];
  selectEcommerce: any = [];
  slectWebFrontEnd: any[] = []
  slectWebBacktEnd: any[] = []
  selectMarketing: any[] = []
  isExportingCSV = false;
  isImportingCSV1 = false;
  staffData: any;
  pdfUrl: any;
  config = {
    keyboard: true,
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'price-break-up car-lead-crm modal-xl ',
    id: 1
  };
  config2 = {
    keyboard: true,
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'price-break-up car-lead-crm modal-sm',
    id: 2
  };
  config3 = {
    keyboard: true,
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'price-break-up car-lead-crm modal-xl',
    id: 3
  };
  // public addressForm: FormGroup;
  isDisabled: boolean = true
  totalRecordsofPendings = 0;
  totalRecordsofHot: any;
  totalRecordsofCold: any;
  totalRecordsOfLost: any;
  totalRecordsofFollowUp: any;
  totalRecordsofWarm: any;
  totalRecordsOfInvoice: any;
  totalRecordsOfconvertedLead: any;
  id: any;
  totalHours: number = 0;
  // ****mobile work****
  allMobileAdd: any = 0;
  valueMobileFlutter: number = 0;
  valueMobileReactNative: number = 0;
  valueMobileKotlin: number = 0;
  valueMobileSwift: number = 0;
  valueMobileRuby: number = 0;
  valueMobileRust: number = 0;
  valueMobileLua: number = 0;
  valueMobileDart: number = 0;
  valueMobileActionScript: number = 0;
  // *****frontendwork***
  allfrontAdd: any = 0;
  valueFrontEndAngular: number = 0;
  valueFrontEndReact: number = 0;
  valueFrontEndVue: number = 0;
  valueFrontEndHtml: number = 0;
  valueFrontEndCss: number = 0;
  valueFrontEndJavascript: number = 0;
  valueFrontEndJquery: number = 0;

  // ****backendwork*****
  allBackAdd: any = 0;
  valueBackEndNodejs: number = 0;
  valueBackEndNestJs: number = 0;
  valueBackEndPython: number = 0;
  valueBackEndPhp: number = 0;
  valueBackEndJava: number = 0;
  valueBackEndPerl: number = 0;
  // ****testingwork***
  allTestAdd: any = 0;
  valueTestingManualTesting: number = 0;
  valueTestingAutomatedTesting: number = 0;
  valueTestingMobileDeviceTesting: number = 0;
  valueTestingFunctionalTesting: number = 0;
  valueTestingUsabilityTesting: number = 0;
  valueTestingCompatibilityTesting: number = 0;
  valueTestingPerformanceAndLoadTesting: number = 0;
  valueTestingSecurityTesting: number = 0;
  // *****design work***
  allDesignAdd: any = 0;
  valueDesginAdobeXD: number = 0;
  valueDesginPhotopea: number = 0;
  valueDesginPhotoshop: number = 0;
  valueDesginFigma: number = 0;
  // *****Ecommerce work***
  allEcommerceAdd: any = 0;
  valueecommerceFrontEnd: number = 0;
  valueecommerceBackEnd: number = 0;


  // ****markting work**
  allMarketingAdd: any = 0;
  valueMarketingSEO: number = 0
  valueMarketingSMO: number = 0
  valueMarketingContent: number = 0
  valueMarketingSMM: number = 0
  valueMarketingGMB: number = 0
  valueMarketingGoogleAbs: number = 0
  valueMarketingGoogleAnalytics: number = 0
  valueMarketingOthers: number = 0


  imgURL: any;
  attachmentData: any;
  oldInputVal: any;
  allEstimate: any;
  leads: any;
  date: any;
  mobileCounter: any = 0;
  emailCounter: any = 0;
  commentLength: any;
  leadStatusData: any;
  mobileHoursArray: any = [];
  totalSubHours: number = 0;
  numberType: any;
  emailType: any;
  googleType: any;
  number1Type: any;
  crossIndex: any;
  allmobilePatch: any;
  oldMobileValue: any;
  submitted = false;
  docCounter: any = 0;
  googleDocArray: any = [1];
  staffsData: any;
  paramsID: any;
  startDate: any;
  endDate: any;
  data: any = [];

  openSiderBar() {
    this.showSiderBar = !this.showSiderBar
  }

  salesCallerCRMForm = this.fb.group({
    salesteleLeadStatus: [''],
    followUpDate: [''],
    followUpTime: [''],
    technology: [''],
    Name: [],
    Phone: [],
    Email: [],
    mobile: [''],
    multiplePh: [],
    multipleEmail: [],
    googleDoc: [],
    Testing: [''],
    Design: [''],
    Ecommerce: [''],
    webfront: [''],
    webback: [''],
    marketing: [''],
    quotationStatus: [''],
    remarks: ['Follow-up'],
    jobTitle: [],
    comments: [],
    requirements: [],
    monthWise: [''],
    newComments: [],
    Estimate: [],
    meetingTime: [],
    attachment: [''],
    type: [],
    Flutter: [],
    ReactNative: [],
    Kotlin: [],
    Swift: [],
    Ruby: [],
    Rust: [],
    Lua: [],
    Dart: [],
    ActionScript: [],
    Angular: [],
    React: [],
    Vue: [],
    Html: [],
    Css: [],
    Javascript: [],
    Jquery: [],
    Nodejs: [],
    NestJs: [],
    Python: [],
    Php: [],
    Java: [],
    Perl: [],
    ManualTesting: [],
    AutomatedTesting: [],
    MobileDeviceTesting: [],
    FunctionalTesting: [],
    UsabilityTesting: [],
    CompatibilityTesting: [],
    PerformanceAndLoadTesting: [],
    SecurityTesting: [],
    AdobeXD: [],
    Photopea: [],
    Photoshop: [],
    Figma: [],
    FrontEnd: [],
    BackEnd: [],
    SEO: [],
    SMO: [],
    Content: [],
    SMM: [],
    GMB: [],
    GoogleAbs: [],
    GoogleAnalytics: [],
    Others: [],

  });
  bsValue: any
  mobileInfoForm = this.fb.group({
  })
  staffLeadForm = this.fb.group({
    Name: ['', Validators.required],
    Phone: ['', Validators.required],
    ProjectDetails: ['', Validators.required],
    source: ['Referrals', Validators.required],
    Location: ['', Validators.required],
    Email: [''],
    Status: ['']
  })
  openInput(value: any) {
    this.hideInput = value;
  }
  userData() {

  }
  sendLeadData(staffData: any) {
    const data = {
      CxName: staffData.value.Name,
      Phonenumber: staffData.value.Phone,
      Email: staffData.value.Email,
      Location: staffData.value.Location,
      leadId: '',
      salesteleLeadStatus: staffData.value.Status || 'pending',
      source: staffData.value.source,
      ProjectDetails: staffData.value.ProjectDetails,
      salesTeleId: '',
      id: this.id,
    }
    this.httpService.findPaticularMobileNo(data.Phonenumber).subscribe(res => {
      this.data = res
      console.log('this.data ', this.data);
      if (this.data.result.length == 0) {


        this.httpService.uploadParticulrData(data).subscribe(res => {
          if (res) {
            this.toasterService.success("lead successfully added");
            this.pending();
          }
        })
        this.staffLeadForm.reset();
      }
      else {
        alert('This Mobile number is Already exists')
      }
    })


  }
  keyPressNumbers(event: any) {
    var charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode < 48 || charCode > 57)) {
      event.preventDefault();
      return false;
    } else {
      return true;
    }
  }
  inputAdd(valueData: any) {
    this.mobileBox = true;
    switch (valueData) {
      case 'mobileNo':
        this.mobileCounter++;
        this.phNumberArray.push(this.mobileCounter);
        this.numberType = typeof (this.phNumberArray[0]);
        break;
      case 'emailData':
        this.emailCounter++;
        this.emailArray.push(this.emailCounter);
        this.emailType = typeof (this.emailArray[0]);
        break;
      case 'googleDoc':
        this.docCounter++;
        this.googleDocArray.push(this.docCounter);
      // this.googleType = typeof (this.googleDocArray[0]);

    }
  }
  removeNumber(indexValue: any, removeValue: any) {
    this.mobileCounter = 0;
    this.emailCounter = 0;
    this.docCounter = 0;
    if (removeValue == 'mobileRemove') {
      this.phNumberArray.splice(indexValue, 1)
      this.allPhNumber.splice(indexValue, 1);
    }
    if (removeValue == 'emailRemove') {
      this.emailArray.splice(indexValue, 1);
      this.allEmaiArray.splice(indexValue, 1);
    }
    if (removeValue == 'googleDoc') {
      this.googleDocArray.splice(indexValue, 1);
      this.allGoogleDocArray.splice(indexValue + 1, 1);
    }
  }
  getPhValue(eventValue: any, inputValue: any) {
    if (inputValue == 'inputValue') {
      this.allPhNumber.push(eventValue.target.value);
    }
    if (inputValue == 'emailValue') {
      this.allEmaiArray.push(eventValue.target.value)
    }
    if (inputValue == 'googleDoc') {
      this.allGoogleDocArray.push(eventValue.target.value);
    }
  }

  dropdownSettings = {
    singleSelection: false,
    idField: 'id',
    textField: 'name',
    // selectAllText: 'Select All',
    // unSelectAllText: 'UnSelect All',
    itemsShowLimit: 1,
    allowSearchFilter: true,
    enableCheckAll: true,
    closeDropDownOnSelection: true,
  };
  selectItem: any;
  mobilevalue: any;
  webFrontValue: any;
  webBackEndValue: any;
  marketingValue: any;
  TestingValue: any;
  designValue: any;
  ecommerceValue: any;
  hotLeadsData: any;
  userEmail: any;
  constructor(private csvService: CSVService, private httpService: HttpService,
    private modalService: BsModalService, private fb: FormBuilder, private route: ActivatedRoute,
    private router: Router, private toasterService: ToastrService
  ) {
    // this.addressForm = this.fb.group({
    //   addresses: this.fb.array([this.createAddress()])
    // });


  }
  createAddress(): FormGroup {
    return this.fb.group({
      address: '',
      street: '',
      city: '',
      country: ''
    });
  }

  ngOnInit() {
    this.id = localStorage.getItem('staffId');
    this.hotLeads();
    this.coldLeads();
    this.warmLeads();
    this.lostLeads();
    this.httpService.getstafflist('getUser').subscribe(res => {
      this.staffsData = res;
      this.loader = false;
    })
    this.route.params.subscribe((params: any) => {
      if (params.id) {
        this.paramsID = params.id
        this.id = params.id;
      }
    });
    this.loadAll()
    this.changeStatus('pending');
    this.googleType = typeof (this.googleDocArray[0]);
  }

  logout() {
    localStorage.removeItem('stafflogin')
    localStorage.clear()
    this.router.navigate(['/'])
  }
  hoursInputData(event: any, id: any) {
  }

  loadAll() {
    this.httpService.pendingLeads(this.id, this.currentPage, 50).subscribe((res: any) => {
      this.staffData = res.result
    })
  }


  exportLeadCSV() {
    this.isExportingCSV = true;
    const csvData = [];
    {
      const csvRow = {
        Name: '',
        Phone: '',
        Email: '',
        Location: '',
      };
      csvData.push(csvRow);
      this.csvService.downloadFile(csvData, 'SalesCsv', ['Name', 'Phone', 'Email', 'Location']);
      this.isExportingCSV = false;
    }
  }

  onCSVSelected(event: any) {
    const files = event.target.files

    if (files && files.length > 0) {
      this.importCSV(files[0]);
    }
  }
  importCSV(csvFile: any) {
    this.isImportingCSV1 = true;
    this.csvService.csv2Array(csvFile).then((data: any[]) => {
      this.httpService.uploadCsvData(data).subscribe(() => {
        this.isImportingCSV1 = false;
      })
    })
  }

  openModal(template: TemplateRef<any>, i: any, value: any) {
    this.crossIndex = i
    this.indexData = this.staffData[i];
    this.leadStatusData = this.staffData[i].salesteleLeadStatus || '',
      this.commentLength = this.indexData.comments?.split(",");
    this.inputName = this.indexData.CxName;
    this.phnoneField = this.indexData?.Phonenumber;
    this.emailField = this.indexData?.Email;
    this.userEmail = this.indexData?.Email;
    let data = this.indexData
    this.modalRef = this.modalService.show(template, this.config);
    this.patchData(i, value);
  }
  openAttachemnt(template: TemplateRef<any>,) {
    this.modalRef1 = this.modalService.show(template, this.config2);
  }
  openLeadPopUp(template: TemplateRef<any>) {
    this.modalRef2 = this.modalService.show(template, this.config3);
  }

  patchData(i: any, value: any) {
    this.date = new Date(this.staffData[i].followUpDate)
    this.indexData.mobile = this.staffData[i].mobile;
    var status = this.staffData[i].salesteleLeadStatus;
    if (this.staffData[i].salesteleLeadStatus == 'pending') {
      status = '';
    }
    this.salesCallerCRMForm.patchValue({
      followUpTime: this.staffData[i].followUpTime || '',
      salesteleLeadStatus: status || '',
      quotationStatus: this.staffData[i].quotationStatus || '',
      comments: this.staffData[i].comments || '',
      remarks: this.staffData[i].remarks || 'Follow-up',
      followUpDate: getPackageDate(this.staffData[i].followUpDate || ''),
      technology: this.staffData[i].technology || '',
      jobTitle: this.staffData[i].jobTitle || '',
      mobile: this.staffData[i].mobile || '',
      webback: this.staffData[i].webback || '',
      webfront: this.staffData[i].webfront || '',
      requirements: this.staffData[i].requirements || '',
      monthWise: this.staffData[i].monthWise || '',
      Estimate: this.staffData[i].Estimate || '',
      meetingTime: this.staffData[i].meetingTime || '',
      Ecommerce: this.staffData[i].Ecommerce || '',
      Design: this.staffData[i].Design || '',
      Testing: this.staffData[i].Testing || '',
      attachment: this.staffData[i].attachment || '',
      type: this.staffData[i].type || '',
      Flutter: this.staffData[i].Flutter || '',
      ReactNative: this.staffData[i].ReactNative || '',
      Kotlin: this.staffData[i].Kotlin || '',
      Swift: this.staffData[i].Swift || '',
      Ruby: this.staffData[i].Ruby || '',
      Rust: this.staffData[i].Rust || '',
      Lua: this.staffData[i].Lua || '',
      Dart: this.staffData[i].Dart || '',
      ActionScript: this.staffData[i].ActionScript || '',
      Angular: this.staffData[i].Angular || '',
      React: this.staffData[i].React || '',
      Vue: this.staffData[i].Vue || '',
      Html: this.staffData[i].Html || '',
      Css: this.staffData[i].Css || '',
      Javascript: this.staffData[i].Javascript || '',
      Jquery: this.staffData[i].Jquery || '',
      Nodejs: this.staffData[i].Nodejs || '',
      NestJs: this.staffData[i].NestJs || '',
      Python: this.staffData[i].Python || '',
      Php: this.staffData[i].Php || '',
      Java: this.staffData[i].Java || '',
      Perl: this.staffData[i].Perl || '',
      ManualTesting: this.staffData[i].ManualTesting || '',
      AutomatedTesting: this.staffData[i].AutomatedTesting || '',
      MobileDeviceTesting: this.staffData[i].MobileDeviceTesting || '',
      FunctionalTesting: this.staffData[i].FunctionalTesting || '',
      UsabilityTesting: this.staffData[i].UsabilityTesting || '',
      CompatibilityTesting: this.staffData[i].CompatibilityTesting || '',
      PerformanceAndLoadTesting: this.staffData[i].PerformanceAndLoadTesting || '',
      SecurityTesting: this.staffData[i].SecurityTesting || '',
      AdobeXD: this.staffData[i].AdobeXD || '',
      Photopea: this.staffData[i].Photopea || '',
      Photoshop: this.staffData[i].Photoshop || '',
      Figma: this.staffData[i].Figma || '',
      FrontEnd: this.staffData[i].FrontEnd || '',
      BackEnd: this.staffData[i].BackEnd || '',
      SEO: this.staffData[i].SEO || '',
      SMO: this.staffData[i].SMO || '',
      Content: this.staffData[i].Content || '',
      SMM: this.staffData[i].SMM || '',
      GMB: this.staffData[i].GMB || '',
      GoogleAbs: this.staffData[i].GoogleAbs || '',
      GoogleAnalytics: this.staffData[i].GoogleAnalytics || '',
      Others: this.staffData[i].Others || '',
    });
    if (this.staffData[i].attachment) {
      if (this.staffData[i].type == "image/png" || this.staffData[i].type == "image/gif" || this.staffData[i].type == "image/jpeg") {
        this.imgURL = this.staffData[i].attachmentUrl;
      }
      if (this.staffData[i].type == "application/pdf")
        this.pdfUrl = this.staffData[i].attachmentUrl;
    }

    if (value == 'followup' || value == 'HOT' || value == 'COLD' || value == 'WARM' || value == 'LOST') {
      this.selectMobile = [...this.staffData[i]?.mobile];
      this.slectWebFrontEnd = [...this.staffData[i]?.webfront];
      this.slectWebBacktEnd = [...this.staffData[i]?.webback];
      this.selectDesign = [...this.staffData[i]?.Design];
      this.selectEcommerce = [...this.staffData[i]?.Ecommerce];
      this.selectTesting = [...this.staffData[i]?.Testing];
      this.selectMarketing = [...this.staffData[i]?.marketing];
      this.selectTech = [...this.staffData[i]?.technology];
      // ****mobile patch here***
      this.valueMobileFlutter = this.staffData[i].Flutter;
      this.valueMobileReactNative = this.staffData[i].ReactNative;
      this.valueMobileKotlin = this.staffData[i].Kotlin;
      this.valueMobileSwift = this.staffData[i].Swift;
      this.valueMobileRuby = this.staffData[i].Ruby;
      this.valueMobileRust = this.staffData[i].Rust;
      this.valueMobileLua = this.staffData[i].Lua;
      this.valueMobileDart = this.staffData[i].Dart;
      this.valueMobileActionScript = this.staffData[i].ActionScript;
      this.allMobileAdd = (this.valueMobileFlutter + this.valueMobileReactNative + this.valueMobileKotlin + this.valueMobileSwift + this.valueMobileRuby + this.valueMobileRust + this.valueMobileLua + this.valueMobileDart + this.valueMobileActionScript)

      // ****frontend patch here***
      this.valueFrontEndAngular = this.staffData[i].Angular;
      this.valueFrontEndReact = this.staffData[i].React;
      this.valueFrontEndVue = this.staffData[i].Vue;
      this.valueFrontEndHtml = this.staffData[i].Html;
      this.valueFrontEndCss = this.staffData[i].Css;
      this.valueFrontEndJavascript = this.staffData[i].Javascript;
      this.valueFrontEndJquery = this.staffData[i].Jquery;
      this.allfrontAdd = (this.valueFrontEndAngular + this.valueFrontEndReact + this.valueFrontEndVue + this.valueFrontEndHtml + this.valueFrontEndCss + this.valueFrontEndJavascript + this.valueFrontEndJquery);

      // ****backend patch here***
      this.valueBackEndNodejs = this.staffData[i].Nodejs;
      this.valueBackEndNestJs = this.staffData[i].NestJs;
      this.valueBackEndPython = this.staffData[i].Python;
      this.valueBackEndPhp = this.staffData[i].Php;
      this.valueBackEndJava = this.staffData[i].Java;
      this.valueBackEndPerl = this.staffData[i].Perl;
      this.allBackAdd = (this.valueBackEndNodejs + this.valueBackEndNestJs + this.valueBackEndPython + this.valueBackEndPhp + this.valueBackEndJava + this.valueBackEndPerl);


      // *****testing patch here****
      this.valueTestingManualTesting = this.staffData[i].ManualTesting;
      this.valueTestingAutomatedTesting = this.staffData[i].AutomatedTesting;
      this.valueTestingMobileDeviceTesting = this.staffData[i].MobileDeviceTesting;
      this.valueTestingFunctionalTesting = this.staffData[i].FunctionalTesting;
      this.valueTestingUsabilityTesting = this.staffData[i].UsabilityTesting;
      this.valueTestingCompatibilityTesting = this.staffData[i].CompatibilityTesting;
      this.valueTestingPerformanceAndLoadTesting = this.staffData[i].PerformanceAndLoadTesting;
      this.valueTestingSecurityTesting = this.staffData[i].SecurityTesting;
      this.allTestAdd = (this.valueTestingManualTesting + this.valueTestingAutomatedTesting + this.valueTestingMobileDeviceTesting + this.valueTestingFunctionalTesting + this.valueTestingUsabilityTesting + this.valueTestingCompatibilityTesting + this.valueTestingPerformanceAndLoadTesting + this.valueTestingSecurityTesting);
      // desing patch here***
      this.valueDesginAdobeXD = this.staffData[i].AdobeXD;
      this.valueDesginPhotopea = this.staffData[i].Photopea;
      this.valueDesginPhotoshop = this.staffData[i].Photoshop;
      this.valueDesginFigma = this.staffData[i].Figma;
      this.allDesignAdd = (this.valueDesginAdobeXD + this.valueDesginPhotopea + this.valueDesginPhotoshop + this.valueDesginFigma)
      // ecommerce patch here****
      this.valueecommerceFrontEnd = this.staffData[i].FrontEnd;
      this.valueecommerceBackEnd = this.staffData[i].BackEnd;
      this.allEcommerceAdd = (this.valueecommerceFrontEnd + this.valueecommerceBackEnd);
      // marketing patch here
      this.valueMarketingSEO = this.staffData[i].SEO;
      this.valueMarketingSMO = this.staffData[i].SMO;
      this.valueMarketingContent = this.staffData[i].Content;
      this.valueMarketingSMM = this.staffData[i].SMM;
      this.valueMarketingGMB = this.staffData[i].GMB;
      this.valueMarketingGoogleAbs = this.staffData[i].GoogleAbs;
      this.valueMarketingGoogleAnalytics = this.staffData[i].GoogleAnalytics;
      this.valueMarketingOthers = this.staffData[i].Others;
      this.allMarketingAdd = (this.valueMarketingSEO + this.valueMarketingSMO + this.valueMarketingContent + this.valueMarketingSMM + this.valueMarketingGMB + this.valueMarketingGoogleAbs + this.valueMarketingGoogleAnalytics + this.valueMarketingOthers)
      this.mobileBox = true;
      this.phNumberArray = [...this.staffData[i].multipleMobile];
      this.allPhNumber = [...this.staffData[i].multipleMobile];
      this.googleDocArray = [...this.staffData[i]?.googleDoc]
      this.numberType = typeof (this.allPhNumber[0]);
      this.googleType = typeof (this.googleDocArray[1]);
      this.emailArray = [...this.staffData[i].multipleEmail];
      this.allEmaiArray = [...this.staffData[i].multipleEmail];
      this.allGoogleDocArray = [...this.staffData[i]?.googleDoc]

      this.emailType = typeof (this.emailArray[0]);
      this.mobilevalue = this.staffData[i]?.mobile.length > 0;
      this.webFrontValue = this.staffData[i]?.webfront.length > 0;
      this.webBackEndValue = this.staffData[i]?.webback.length > 0;
      this.TestingValue = this.staffData[i]?.Testing.length > 0;
      this.designValue = this.staffData[i]?.Design.length > 0;
      this.ecommerceValue = this.staffData[i]?.Ecommerce.length > 0;
      this.marketingValue = this.staffData[i]?.marketing.length > 0;
    }
    if (value == 'pending') {
      this.phNumberArray = [];
      this.valueMobileFlutter = 0;
      this.valueMobileReactNative = 0;
      this.valueMobileKotlin = 0;
      this.valueMobileSwift = 0;
      this.valueMobileRuby = 0;
      this.valueMobileRust = 0;
      this.valueMobileLua = 0;
      this.valueMobileDart = 0;
      this.valueMobileActionScript = 0;
      this.allMobileAdd = 0;
      // ****frontend patch here***
      this.valueFrontEndAngular = 0;
      this.valueFrontEndReact = 0;
      this.valueFrontEndVue = 0;
      this.valueFrontEndHtml = 0;
      this.valueFrontEndCss = 0;
      this.valueFrontEndJavascript = 0;
      this.valueFrontEndJquery = 0;
      this.allfrontAdd = 0;
      // ****backend patch here***
      this.valueBackEndNodejs = 0;
      this.valueBackEndNestJs = 0;
      this.valueBackEndPython = 0;
      this.valueBackEndPhp = 0;
      this.valueBackEndJava = 0;
      this.valueBackEndPerl = 0;
      this.allBackAdd = 0;
      // *****testing patch here****
      this.valueTestingManualTesting = 0;
      this.valueTestingAutomatedTesting = 0;
      this.valueTestingMobileDeviceTesting = 0;
      this.valueTestingFunctionalTesting = 0;
      this.valueTestingUsabilityTesting = 0;
      this.valueTestingCompatibilityTesting = 0;
      this.valueTestingPerformanceAndLoadTesting = 0;
      this.valueTestingSecurityTesting = 0;
      this.allTestAdd = 0;
      // desing patch here***
      this.valueDesginAdobeXD = 0;
      this.valueDesginPhotopea = 0;
      this.valueDesginPhotoshop = 0;
      this.valueDesginFigma = 0;
      this.allDesignAdd = 0;
      // ecommerce patch here****
      this.valueecommerceFrontEnd = 0;
      this.valueecommerceBackEnd = 0;
      this.allEcommerceAdd = 0;
      // marketing patch here
      this.valueMarketingSEO = 0;
      this.valueMarketingSMO = 0;
      this.valueMarketingContent = 0;
      this.valueMarketingSMM = 0;
      this.valueMarketingGMB = 0;
      this.valueMarketingGoogleAbs = 0;
      this.valueMarketingGoogleAnalytics = 0;
      this.valueMarketingOthers = 0;
      this.allMarketingAdd = 0;
    }

  }
  searchLeads() {
    if (this.leadNumData.value != undefined && this.leadNumData.value != null && this.leadNumData.value != '' && this.leadNumData.value.length >= 4 && this.leadNumData.value.length <= 6) {
      this.leads = this.leadNumData.value
      this.httpService.findPaticularLead(this.leads).subscribe((res: any) => {
        if (res.result == null) {
          this.staffData = [];
        }
        else {
          this.staffData = [res.result];
        }
      })
    }
  }
  // Sanjay
  searchByMobileNumber() {
    if (this.leadNumData.value != undefined && this.leadNumData.value != null && this.leadNumData.value != '') {
      this.leads = this.leadNumData.value
      this.id
      this.httpService.findPaticularBdeMobileNo(this.id, this.leads).subscribe((res: any) => {
        this.staffData = res.result
        //  this.totalRecords=res.totalRecords
      })
    }
  }
  getStatus() {
    this.leadStatusData = this.salesCallerCRMForm.get('salesteleLeadStatus')?.value;
  }

  onChangeDatas(event: any) {
    this.startDate = event[0];
    this.endDate = event[1];

    if (this.startDate && this.endDate) {
      this.httpService.searchByDateRange(this.startDate, this.endDate, this.id, this.saleManageStatus).subscribe((res: any) => {
        this.staffData = res.result
        console.log('dateRaneg', this.staffData);

      })
    }
  }
  hideModel(status: any, crossIndex: any) {
    this.hideInput = '';
    this.mobilevalue = '';
    this.webFrontValue = '';
    this.webBackEndValue = '';
    this.marketingValue = '';
    this.ecommerceValue = '',
      this.designValue = '',
      this.TestingValue = '',
      this.selectMobile = [];
    this.slectWebFrontEnd = [];
    this.slectWebBacktEnd = [];
    this.selectTech = [];
    this.selectTesting = [];
    this.selectDesign = [];
    this.selectEcommerce = [];
    this.selectMarketing = [];
    this.phNumberArray = [];
    this.totalHours = 0;
    this.mobileCounter = 0;
    this.emailCounter = 0;
    this.googleDocArray = [1];
    this.leadStatusData = '';
    this.mobileBox = false;
    this.allPhNumber.length = 0;
    this.emailArray.length = 0;
    this.submitted = false;
    this.imgURL = null;
    this.pdfUrl = null;
    switch (status) {
      case 'pending':
        this.indexData.technology = ''
        this.indexData.mobile = ''
        this.indexData.webfront = ''
        this.indexData.webback = ''
        this.indexData.marketing = ''
        this.indexData.Testing = ''
        this.indexData.Design = ''
        this.indexData.Ecommerce = ''
        break;
      case 'COLD':
    }
    this.totalHours = 0
    this.modalRef.hide();
    this.salesCallerCRMForm.reset();
  }
  // hiddenPopUP() {
  //   this.modalRef2.hide();
  //   this.staffLeadForm.reset();
  // }
  onFileSelect(files: any) {
    this.attachmentData = files[0];
    this.salesCallerCRMForm.patchValue({
      type: files[0].type,
      attachment: files[0].name,
    })
    var reader = new FileReader();
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    }
  }
  // tech//
  onTechSelect(event: any) {
    if (event.name === 'Mobile') {
      this.mobilevalue = event.name
    }
    if (event.name === 'Web') {
      this.webFrontValue = event.name
    }
    if (event.name === 'Web') {
      this.webBackEndValue = event.name
    }
    if (event.name === 'Marketing') {
      this.marketingValue = event.name
    }
    if (event.name === 'Testing') {
      this.TestingValue = event.name
    }
    if (event.name === 'Design') {
      this.designValue = event.name
    }
    if (event.name === 'Ecommerce') {
      this.ecommerceValue = event.name
    }
    // this.selectItem = event
    this.selectTech.push(event);
  }
  onDeSelect(event: any) {
    for (let i = 0; i < this.selectTech.length; i++) {
      if (event.id == this.selectTech[i].id) {
        this.selectTech.splice(i, 1);
      }
    }
    if (event.name === 'Mobile') {
      this.mobilevalue = '';
    }
    if (event.name === 'Web') {
      this.webBackEndValue = ''
      this.webFrontValue = ''
    }
    if (event.name === 'Marketing') {
      this.marketingValue = '';
    }
    if (event.name === 'Testing') {
      this.TestingValue = '';
    }
    if (event.name === 'Design') {
      this.designValue = '';
    }
    if (event.name === 'Ecommerce') {
      this.ecommerceValue = '';
    }
  }

  // Mobile
  onMobileSelect(event: any) {
    if (this.selectMobile.length < 5)
      this.selectMobile.push({ id: event.id, name: event.name });
  }
  onTestingSelect(event: any) {
    if (this.selectTesting.length < 5)
      this.selectTesting.push({ id: event.id, name: event.name });
  }
  onDesignSelect(event: any) {
    if (this.selectDesign.length < 5)
      this.selectDesign.push({ id: event.id, name: event.name });
  }
  onEcommerceSelect(event: any) {
    if (this.selectEcommerce.length < 5)
      this.selectEcommerce.push({ id: event.id, name: event.name });
  }
  onEcommerceDeSelect(event: any) {
    switch (event?.name) {
      case "FrontEnd":
        this.allEcommerceAdd = this.allEcommerceAdd - this.valueecommerceFrontEnd;
        this.valueecommerceFrontEnd = 0
        this.salesCallerCRMForm.patchValue({
          FrontEnd: 0,
        })
        this.patchEstimateFun();
        break;
      case "BackEnd":
        this.allEcommerceAdd = this.allEcommerceAdd - this.valueecommerceBackEnd;
        this.valueecommerceBackEnd = 0;
        this.salesCallerCRMForm.patchValue({
          BackEnd: 0,
        })
        this.patchEstimateFun();
        break;
    }
    for (let i = 0; i < this.selectEcommerce.length; i++) {
      if (event.id == this.selectEcommerce[i].id) {
        this.selectEcommerce.splice(i, 1);
      }
    }
  }
  onDesignDeSelect(event: any) {
    switch (event?.name) {
      case "AdobeXD":
        this.allDesignAdd = this.allDesignAdd - this.valueDesginAdobeXD;
        this.valueDesginAdobeXD = 0
        this.salesCallerCRMForm.patchValue({
          AdobeXD: 0,
        })
        this.patchEstimateFun();
        break;
      case "Photopea":
        this.allDesignAdd = this.allDesignAdd - this.valueDesginPhotopea;
        this.valueDesginPhotopea = 0;
        this.salesCallerCRMForm.patchValue({
          Photopea: 0,
        })
        this.patchEstimateFun();
        break;
      case "Photoshop":
        this.allDesignAdd = this.allDesignAdd - this.valueDesginPhotoshop;
        this.valueDesginPhotoshop = 0;
        this.salesCallerCRMForm.patchValue({
          Photoshop: 0,
        })
        this.patchEstimateFun();
        break;
      case "Figma":
        this.allDesignAdd = this.allDesignAdd - this.valueDesginFigma;
        this.valueDesginFigma = 0;
        this.salesCallerCRMForm.patchValue({
          Figma: 0,
        })
        this.patchEstimateFun();
        break;
    }
    for (let i = 0; i < this.selectDesign.length; i++) {
      if (event.id == this.selectDesign[i].id) {
        this.selectDesign.splice(i, 1);
      }
    }
  }
  onTestingDeSelect(event: any) {
    switch (event?.name) {
      case "ManualTesting":
        this.allTestAdd = this.allTestAdd - this.valueTestingManualTesting;
        this.valueTestingManualTesting = 0
        this.salesCallerCRMForm.patchValue({
          ManualTesting: 0,
        })
        this.patchEstimateFun();
        break;
      case "AutomatedTesting":
        this.allTestAdd = this.allTestAdd - this.valueTestingAutomatedTesting;
        this.valueTestingAutomatedTesting = 0;
        this.salesCallerCRMForm.patchValue({
          AutomatedTesting: 0,
        })
        this.patchEstimateFun();
        break;
      case "MobileDeviceTesting":
        this.allTestAdd = this.allTestAdd - this.valueTestingMobileDeviceTesting;
        this.valueTestingMobileDeviceTesting = 0;
        this.salesCallerCRMForm.patchValue({
          MobileDeviceTesting: 0,
        })
        this.patchEstimateFun();
        break;
      case "FunctionalTesting":
        this.allTestAdd = this.allTestAdd - this.valueTestingFunctionalTesting;
        this.valueTestingFunctionalTesting = 0;
        this.salesCallerCRMForm.patchValue({
          FunctionalTesting: 0,
        })
        this.patchEstimateFun();
        break;
      case "UsabilityTesting":
        this.allTestAdd = this.allTestAdd - this.valueTestingUsabilityTesting;
        this.valueTestingUsabilityTesting = 0;
        this.salesCallerCRMForm.patchValue({
          UsabilityTesting: 0,
        })
        this.patchEstimateFun();
        break;
      case "CompatibilityTesting":
        this.allTestAdd = this.allTestAdd - this.valueTestingCompatibilityTesting;
        this.valueTestingCompatibilityTesting = 0;
        this.salesCallerCRMForm.patchValue({
          CompatibilityTesting: 0,
        })
        this.patchEstimateFun();
        break;
      case "PerformanceAndLoadTesting":
        this.allTestAdd = this.allTestAdd - this.valueTestingPerformanceAndLoadTesting;
        this.valueTestingPerformanceAndLoadTesting = 0
        this.salesCallerCRMForm.patchValue({
          PerformanceAndLoadTesting: 0,
        })
        this.patchEstimateFun();
        break;
      case "SecurityTesting":
        this.allTestAdd = this.allTestAdd - this.valueTestingSecurityTesting;
        this.valueTestingSecurityTesting = 0;
        this.salesCallerCRMForm.patchValue({
          SecurityTesting: 0,
        })
        this.patchEstimateFun();
        break;
    }
    for (let i = 0; i < this.selectTesting.length; i++) {
      if (event.id == this.selectTesting[i].id) {
        this.selectTesting.splice(i, 1);
      }
    }
  }
  onMobileDeSelect(event: any) {
    switch (event?.name) {
      case "Flutter":
        this.allMobileAdd = this.allMobileAdd - this.valueMobileFlutter;
        this.valueMobileFlutter = 0
        this.salesCallerCRMForm.patchValue({
          Flutter: 0,
        })
        this.patchEstimateFun();
        break;
      case "ReactNative":
        this.allMobileAdd = this.allMobileAdd - this.valueMobileReactNative;
        this.valueMobileReactNative = 0;
        this.salesCallerCRMForm.patchValue({
          ReactNative: 0,
        })
        this.patchEstimateFun();
        break;
      case "Kotlin":
        this.allMobileAdd = this.allMobileAdd - this.valueMobileKotlin;
        this.valueMobileKotlin = 0;
        this.salesCallerCRMForm.patchValue({
          Kotlin: 0,
        })
        this.patchEstimateFun();
        break;
      case "Swift":
        this.allMobileAdd = this.allMobileAdd - this.valueMobileSwift;
        this.valueMobileSwift = 0;
        this.salesCallerCRMForm.patchValue({
          Swift: 0,
        })
        this.patchEstimateFun();
        break;
      case "Ruby":
        this.allMobileAdd = this.allMobileAdd - this.valueMobileRuby;
        this.valueMobileRuby = 0;
        this.salesCallerCRMForm.patchValue({
          Ruby: 0,
        })
        this.patchEstimateFun();
        break;
      case "Rust":
        this.allMobileAdd = this.allMobileAdd - this.valueMobileRust;
        this.valueMobileRust = 0;
        this.salesCallerCRMForm.patchValue({
          Rust: 0,
        })
        this.patchEstimateFun();
        break;
      case "Lua":
        this.allMobileAdd = this.allMobileAdd - this.valueMobileLua;
        this.valueMobileLua = 0
        this.salesCallerCRMForm.patchValue({
          Lua: 0,
        })
        this.patchEstimateFun();
        break;
      case "ActionScript":
        this.allMobileAdd = this.allMobileAdd - this.valueMobileActionScript;
        this.valueMobileActionScript = 0;
        this.salesCallerCRMForm.patchValue({
          ActionScript: 0,
        })
        this.patchEstimateFun();
        break;
    }
    for (let i = 0; i < this.selectMobile.length; i++) {
      if (event.id == this.selectMobile[i].id) {
        this.selectMobile.splice(i, 1);
      }
    }
  }
  onwebFrontEndSelect(event: any) {
    if (this.slectWebFrontEnd.length < 5)
      this.slectWebFrontEnd.push({ id: event.id, name: event.name });
  }

  onwebFrontEndDeSelect(event: any) {
    switch (event?.name) {
      case "Angular":
        this.allfrontAdd = this.allfrontAdd - this.valueFrontEndAngular;
        this.valueFrontEndAngular = 0
        this.salesCallerCRMForm.patchValue({
          Angular: 0,
        })
        this.patchEstimateFun();
        break;
      case "React":
        this.allfrontAdd = this.allfrontAdd - this.valueFrontEndReact;
        this.valueFrontEndReact = 0;
        this.salesCallerCRMForm.patchValue({
          React: 0,
        })
        this.patchEstimateFun();
        break;
      case "Vue":
        this.allfrontAdd = this.allfrontAdd - this.valueFrontEndVue;
        this.valueFrontEndVue = 0;
        this.salesCallerCRMForm.patchValue({
          Vue: 0,
        })
        this.patchEstimateFun();
        break;
      case "Html":
        this.allfrontAdd = this.allfrontAdd - this.valueFrontEndHtml;
        this.valueFrontEndHtml = 0;
        this.salesCallerCRMForm.patchValue({
          Html: 0,
        })
        this.patchEstimateFun();
        break;
      case "Css":
        this.allfrontAdd = this.allfrontAdd - this.valueFrontEndCss;
        this.valueFrontEndCss = 0;
        this.salesCallerCRMForm.patchValue({
          Css: 0,
        })
        this.patchEstimateFun();
        break;
      case "Javascript":
        this.allfrontAdd = this.allfrontAdd - this.valueFrontEndJavascript;
        this.valueFrontEndJavascript = 0;
        this.salesCallerCRMForm.patchValue({
          Javascript: 0,
        })
        this.patchEstimateFun();
        break;
      case "Jquery":
        this.allfrontAdd = this.allfrontAdd - this.valueFrontEndJquery;
        this.valueFrontEndJquery = 0;
        this.salesCallerCRMForm.patchValue({
          Jquery: 0,
        })
        this.patchEstimateFun();
        break;
    }
    for (let i = 0; i < this.slectWebFrontEnd.length; i++) {
      if (event.id == this.slectWebFrontEnd[i].id) {
        this.slectWebFrontEnd.splice(i, 1);
      }
    }
  }

  // WebBackEnd//
  onwebBackEndSelect(event: any) {
    if (this.slectWebBacktEnd.length < 5)
      this.slectWebBacktEnd.push({ id: event.id, name: event.name });
  }

  onwebBackEndDeSelect(event: any) {
    switch (event?.name) {
      case "Nodejs":
        this.allBackAdd = this.allBackAdd - this.valueBackEndNodejs;
        this.valueBackEndNodejs = 0
        this.salesCallerCRMForm.patchValue({
          Nodejs: 0,
        })
        this.patchEstimateFun();
        break;
      case "NestJs":
        this.allBackAdd = this.allBackAdd - this.valueBackEndNestJs;
        this.valueBackEndNestJs = 0;
        this.salesCallerCRMForm.patchValue({
          NestJs: 0,
        })
        this.patchEstimateFun();
        break;
      case "Python":
        this.allBackAdd = this.allBackAdd - this.valueBackEndPython;
        this.valueBackEndPython = 0;
        this.salesCallerCRMForm.patchValue({
          Python: 0,
        })
        this.patchEstimateFun();
        break;
      case "Php":
        this.allBackAdd = this.allBackAdd - this.valueBackEndPhp;
        this.valueBackEndPhp = 0;
        this.salesCallerCRMForm.patchValue({
          Php: 0,
        })
        this.patchEstimateFun();
        break;
      case "Java":
        this.allBackAdd = this.allBackAdd - this.valueBackEndJava;
        this.valueBackEndJava = 0;
        this.salesCallerCRMForm.patchValue({
          Java: 0,
        })
        this.patchEstimateFun();
        break;
      case "Perl":
        this.allBackAdd = this.allBackAdd - this.valueBackEndPerl;
        this.valueBackEndPerl = 0;
        this.salesCallerCRMForm.patchValue({
          Perl: 0,
        })
        this.patchEstimateFun();
        break;
    }

    if (event.name === 'Web') {
      this.webBackEndValue = ''
    }
    for (let i = 0; i < this.slectWebBacktEnd.length; i++) {
      if (event.id == this.slectWebBacktEnd[i].id) {
        this.slectWebBacktEnd.splice(i, 1);
      }
    }
  }

  // Marketing
  onMarketelect(event: any) {
    // if (this.selectMarketing.length < 5) {
    //   this.selectMarketing.push({ id: event.id, name: event.name });
    // }
    this.selectMarketing.length < 5 ? this.selectMarketing.push({ id: event.id, name: event.name }) : this.toasterService.success("You Can Select Only Five Input")


  }

  onMarketDeSelect(event: any) {

    this.totalHours = this.allMobileAdd + this.allfrontAdd + this.allBackAdd + this.allTestAdd + this.allDesignAdd + this.allEcommerceAdd + this.allMarketingAdd;
    switch (event?.name) {
      case "SEO":
        this.allMarketingAdd = this.allMarketingAdd - this.valueMarketingSEO;
        this.valueMarketingSEO = 0
        this.salesCallerCRMForm.patchValue({
          SEO: 0,
        })
        this.patchEstimateFun();
        break;
      case "SMO":
        this.allMarketingAdd = this.allMarketingAdd - this.valueMarketingSMO;
        this.valueMarketingSMO = 0
        this.salesCallerCRMForm.patchValue({
          SMO: 0,
        })
        this.patchEstimateFun();
        break;
      case "Content":
        this.allMarketingAdd = this.allMarketingAdd - this.valueMarketingContent;
        this.valueMarketingContent = 0
        this.salesCallerCRMForm.patchValue({
          Content: 0,
        })
        this.patchEstimateFun();
        break;
      case "SMM":
        this.allMarketingAdd = this.allMarketingAdd - this.valueMarketingSMM;
        this.valueMarketingSMM = 0
        this.salesCallerCRMForm.patchValue({
          SMM: 0,
        })
        this.patchEstimateFun();
        break;
      case "GMB":
        this.allMarketingAdd = this.allMarketingAdd - this.valueMarketingGMB;
        this.valueMarketingGMB = 0
        this.salesCallerCRMForm.patchValue({
          GMB: 0,
        })
        this.patchEstimateFun();
        break;
      case "GoogleAbs":
        this.allMarketingAdd = this.allMarketingAdd - this.valueMarketingGoogleAbs;
        this.valueMarketingGoogleAbs = 0
        this.salesCallerCRMForm.patchValue({
          GoogleAbs: 0,
        })
        this.patchEstimateFun();
        break;
      case "GoogleAnalytics":
        this.allMarketingAdd = this.allMarketingAdd - this.valueMarketingGoogleAnalytics;
        this.valueMarketingGoogleAnalytics = 0
        this.salesCallerCRMForm.patchValue({
          GoogleAnalytics: 0,
        })
        this.patchEstimateFun();
        break;
      case "Others":
        this.allMarketingAdd = this.allMarketingAdd - this.valueMarketingOthers;
        this.valueMarketingOthers = 0
        this.salesCallerCRMForm.patchValue({
          Others: 0,
        })
        this.patchEstimateFun();
        break;
    }
    if (event.name === 'Marketing') {
      this.marketingValue = ''
    }
    for (let i = 0; i < this.selectMarketing.length; i++) {
      if (event.id == this.selectMarketing[i].id) {
        this.selectMarketing.splice(i, 1);
      }
    }
  }

  private createForm(data: any) {

    var allCommentsWithDateRecord: any;
    if (this.indexData.comments == undefined || this.indexData.comments == null || this.indexData.comments == '')
      allCommentsWithDateRecord = moment(new Date()).format('YYYY-MM-DD') + '---' + this.salesCallerCRMForm.get('newComments')?.value
    else
      allCommentsWithDateRecord = this.indexData.comments + ',' + moment(new Date()).format('YYYY-MM-DD') + '---' + this.salesCallerCRMForm.get('newComments')?.value
    const userData = {
      id: this.indexData._id,
      Name: this.inputName,
      Phone: this.phnoneField,
      Email: this.emailField,
      followUpDate: this.salesCallerCRMForm.get('followUpDate')?.value,
      followUpTime: this.salesCallerCRMForm.get('followUpTime')?.value,
      salesteleLeadStatus: this.salesCallerCRMForm.get('salesteleLeadStatus')?.value,
      quotationStatus: this.salesCallerCRMForm.get('quotationStatus')?.value,
      remarks: this.salesCallerCRMForm.get('remarks')?.value,
      monthWise: this.salesCallerCRMForm.get('monthWise')?.value,
      Estimate: this.salesCallerCRMForm.get('Estimate')?.value,
      multiplePh: this.allPhNumber,
      multipleEmail: this.allEmaiArray,
      googleDoc: this.allGoogleDocArray,
      technology: this.selectTech,
      webfront: this.slectWebFrontEnd,
      webback: this.slectWebBacktEnd,
      marketing: this.selectMarketing,
      Testing: this.selectTesting,
      Design: this.selectDesign,
      Ecommerce: this.selectEcommerce,
      jobTitle: this.salesCallerCRMForm.get('jobTitle')?.value,
      comments: allCommentsWithDateRecord,
      requirements: this.salesCallerCRMForm.get('requirements')?.value,
      mobile: this.selectMobile,
      meetingTime: this.salesCallerCRMForm.get('meetingTime')?.value,
      attachment: this.salesCallerCRMForm.get('attachment')?.value,
      type: this.salesCallerCRMForm.get('type')?.value,
      Flutter: this.salesCallerCRMForm.get('Flutter')?.value,
      ReactNative: this.salesCallerCRMForm.get('ReactNative')?.value,
      Kotlin: this.salesCallerCRMForm.get('Kotlin')?.value,
      Swift: this.salesCallerCRMForm.get('Swift')?.value,
      Ruby: this.salesCallerCRMForm.get('Ruby')?.value,
      Rust: this.salesCallerCRMForm.get('Rust')?.value,
      Lua: this.salesCallerCRMForm.get('Lua')?.value,
      Dart: this.salesCallerCRMForm.get('Dart')?.value,
      ActionScript: this.salesCallerCRMForm.get('ActionScript')?.value,
      Angular: this.salesCallerCRMForm.get('Angular')?.value,
      React: this.salesCallerCRMForm.get('React')?.value,
      Vue: this.salesCallerCRMForm.get('Vue')?.value,
      Html: this.salesCallerCRMForm.get('Html')?.value,
      Css: this.salesCallerCRMForm.get('Css')?.value,
      Javascript: this.salesCallerCRMForm.get('Javascript')?.value,
      Jquery: this.salesCallerCRMForm.get('Jquery')?.value,
      Nodejs: this.salesCallerCRMForm.get('Nodejs')?.value,
      NestJs: this.salesCallerCRMForm.get('NestJs')?.value,
      Python: this.salesCallerCRMForm.get('Python')?.value,
      Php: this.salesCallerCRMForm.get('Php')?.value,
      Java: this.salesCallerCRMForm.get('Java')?.value,
      Perl: this.salesCallerCRMForm.get('Perl')?.value,
      ManualTesting: this.salesCallerCRMForm.get('ManualTesting')?.value,
      AutomatedTesting: this.salesCallerCRMForm.get('AutomatedTesting')?.value,
      MobileDeviceTesting: this.salesCallerCRMForm.get('MobileDeviceTesting')?.value,
      FunctionalTesting: this.salesCallerCRMForm.get('FunctionalTesting')?.value,
      UsabilityTesting: this.salesCallerCRMForm.get('UsabilityTesting')?.value,
      CompatibilityTesting: this.salesCallerCRMForm.get('CompatibilityTesting')?.value,
      PerformanceAndLoadTesting: this.salesCallerCRMForm.get('PerformanceAndLoadTesting')?.value,
      SecurityTesting: this.salesCallerCRMForm.get('SecurityTesting')?.value,
      AdobeXD: this.salesCallerCRMForm.get('AdobeXD')?.value,
      Photopea: this.salesCallerCRMForm.get('Photopea')?.value,
      Photoshop: this.salesCallerCRMForm.get('Photoshop')?.value,
      Figma: this.salesCallerCRMForm.get('Figma')?.value,
      FrontEnd: this.salesCallerCRMForm.get('FrontEnd')?.value,
      BackEnd: this.salesCallerCRMForm.get('BackEnd')?.value,
      SEO: this.salesCallerCRMForm.get('SEO')?.value,
      SMO: this.salesCallerCRMForm.get('SMO')?.value,
      Content: this.salesCallerCRMForm.get('Content')?.value,
      SMM: this.salesCallerCRMForm.get('SMM')?.value,
      GMB: this.salesCallerCRMForm.get('GMB')?.value,
      GoogleAbs: this.salesCallerCRMForm.get('GoogleAbs')?.value,
      GoogleAnalytics: this.salesCallerCRMForm.get('GoogleAnalytics')?.value,
      Others: this.salesCallerCRMForm.get('Others')?.value,

    };
    return userData;
  }




  mobileValueGet(event: any, i: any, statusCheck: any, selectData: any) {
    switch (selectData) {
      case "Flutter":
        this.valueMobileFlutter = parseInt(event.target?.value) || 0;
        break;
      case "ReactNative":
        this.valueMobileReactNative = parseInt(event.target?.value) || 0;
        break;
      case "Kotlin":
        this.valueMobileKotlin = parseInt(event.target?.value) || 0;
        break;
      case "Swift":
        this.valueMobileSwift = parseInt(event.target?.value) || 0;
        break;
      case "Ruby":
        this.valueMobileRuby = parseInt(event.target?.value) || 0;
        break;
      case "Rust":
        this.valueMobileRust = parseInt(event.target?.value) || 0;
        break;
      case "Lua":
        this.valueMobileLua = parseInt(event.target?.value) || 0;
        break;
      case "Dart":
        this.valueMobileDart = parseInt(event.target?.value) || 0;
        break;
      case "ActionScript":
        this.valueMobileActionScript = parseInt(event.target?.value) || 0;
        break;
    }
    this.allMobileAdd = (this.valueMobileFlutter + this.valueMobileReactNative + this.valueMobileKotlin + this.valueMobileSwift + this.valueMobileRuby + this.valueMobileRust + this.valueMobileLua + this.valueMobileDart + this.valueMobileActionScript);
    this.totalHours = this.allMobileAdd + this.allfrontAdd + this.allBackAdd + this.allTestAdd + this.allDesignAdd + this.allEcommerceAdd + this.allMarketingAdd;
    this.salesCallerCRMForm.patchValue({
      Estimate: this.totalHours,
    })
  }
  frontEndvalueGet(event: any, i: any, WebFrontData: any) {
    switch (WebFrontData) {
      case "Angular":
        this.valueFrontEndAngular = parseInt(event.target.value) || 0;
        break;
      case "React":
        this.valueFrontEndReact = parseInt(event.target.value) || 0;
        break;
      case "Vue":
        this.valueFrontEndVue = parseInt(event.target.value) || 0;
        break;
      case "Html":
        this.valueFrontEndHtml = parseInt(event.target.value) || 0;
        break;
      case "Css":
        this.valueFrontEndCss = parseInt(event.target.value) || 0;
        break;
      case "Javascript":
        this.valueFrontEndJavascript = parseInt(event.target.value) || 0;
        break;
      case "Jquery":
        this.valueFrontEndJquery = parseInt(event.target.value) || 0;
        break;
    }
    this.allfrontAdd = (this.valueFrontEndAngular + this.valueFrontEndReact + this.valueFrontEndVue + this.valueFrontEndHtml + this.valueFrontEndCss + this.valueFrontEndJavascript + this.valueFrontEndJquery);
    this.totalHours = this.allMobileAdd + this.allfrontAdd + this.allBackAdd + this.allTestAdd + this.allDesignAdd + this.allEcommerceAdd + this.allMarketingAdd;
    this.salesCallerCRMForm.patchValue({
      Estimate: this.totalHours,
    })
  }
  testingValueGet(event: any, i: any, selectData: any) {

    switch (selectData) {
      case "ManualTesting":
        this.valueTestingManualTesting = parseInt(event.target.value) || 0;
        break;
      case "AutomatedTesting":
        this.valueTestingAutomatedTesting = parseInt(event.target.value) || 0;
        break;
      case "MobileDeviceTesting":
        this.valueTestingMobileDeviceTesting = parseInt(event.target.value) || 0;
        break;
      case "FunctionalTesting":
        this.valueTestingFunctionalTesting = parseInt(event.target.value) || 0;
        break;
      case "UsabilityTesting":
        this.valueTestingUsabilityTesting = parseInt(event.target.value) || 0;
        break;
      case "CompatibilityTesting":
        this.valueTestingCompatibilityTesting = parseInt(event.target.value) || 0;
        break;
      case "PerformanceAndLoadTesting":
        this.valueTestingPerformanceAndLoadTesting = parseInt(event.target.value) || 0;
        break;
      case "SecurityTesting":
        this.valueTestingSecurityTesting = parseInt(event.target.value) || 0;
        break;
    }
    this.allTestAdd = (this.valueTestingManualTesting + this.valueTestingAutomatedTesting + this.valueTestingMobileDeviceTesting + this.valueTestingFunctionalTesting + this.valueTestingUsabilityTesting + this.valueTestingCompatibilityTesting + this.valueTestingPerformanceAndLoadTesting + this.valueTestingSecurityTesting)
    this.totalHours = this.allMobileAdd + this.allfrontAdd + this.allBackAdd + this.allTestAdd + this.allDesignAdd + this.allEcommerceAdd + this.allMarketingAdd;
    this.salesCallerCRMForm.patchValue({
      Estimate: this.totalHours,
    })
  }
  designValueGet(event: any, i: any, selectData: any) {

    switch (selectData) {
      case "AdobeXD":
        this.valueDesginAdobeXD = parseInt(event.target.value) || 0;
        break;
      case "Photopea":
        this.valueDesginPhotopea = parseInt(event.target.value) || 0;
        break;
      case "Photoshop":
        this.valueDesginPhotoshop = parseInt(event.target.value) || 0;
        break;
      case "Figma":
        this.valueDesginFigma = parseInt(event.target.value) || 0;
        break;
    }
    this.allDesignAdd = (this.valueDesginAdobeXD + this.valueDesginPhotopea + this.valueDesginPhotoshop + this.valueDesginFigma);
    this.totalHours = this.allMobileAdd + this.allfrontAdd + this.allBackAdd + this.allTestAdd + this.allDesignAdd + this.allEcommerceAdd + this.allMarketingAdd;
    this.salesCallerCRMForm.patchValue({
      Estimate: this.totalHours,
    })
  }
  ecommerceValueGet(event: any, i: any, selectData: any) {
    switch (selectData) {
      case "FrontEnd":
        this.valueecommerceFrontEnd = parseInt(event.target.value) || 0;
        break;
      case "BackEnd":
        this.valueecommerceBackEnd = parseInt(event.target.value) || 0;
        break;
    }
    this.allEcommerceAdd = (this.valueecommerceFrontEnd + this.valueecommerceBackEnd);
    this.totalHours = this.allMobileAdd + this.allfrontAdd + this.allBackAdd + this.allTestAdd + this.allDesignAdd + this.allEcommerceAdd + this.allMarketingAdd;
    this.salesCallerCRMForm.patchValue({
      Estimate: this.totalHours,
    })
  }
  backEndValueGet(event: any, i: any, WebBacktData: any) {
    switch (WebBacktData) {
      case "Nodejs":
        this.valueBackEndNodejs = parseInt(event.target.value) || 0;
        break;
      case "NestJs":
        this.valueBackEndNestJs = parseInt(event.target.value) || 0;
        break;
      case "Python":
        this.valueBackEndPython = parseInt(event.target.value) || 0;
        break;
      case "Php":
        this.valueBackEndPhp = parseInt(event.target.value) || 0;
        break;
      case "Java":
        this.valueBackEndJava = parseInt(event.target.value) || 0;
        break;
      case "Perl":
        this.valueBackEndPerl = parseInt(event.target.value) || 0;
        break;
    }
    this.allBackAdd = (this.valueBackEndNodejs + this.valueBackEndNestJs + this.valueBackEndPython + this.valueBackEndPhp + this.valueBackEndJava + this.valueBackEndPerl);
    this.totalHours = this.allMobileAdd + this.allfrontAdd + this.allBackAdd + this.allTestAdd + this.allDesignAdd + this.allEcommerceAdd + this.allMarketingAdd;
    this.salesCallerCRMForm.patchValue({
      Estimate: this.totalHours,
    })
  }
  MarketingDataGet(event: any, MarketingData: any) {
    switch (MarketingData) {
      case "SEO":
        this.valueMarketingSEO = parseInt(event.target.value) || 0;
        break;
      case "SMO":
        this.valueMarketingSMO = parseInt(event.target.value) || 0;
        break;
      case "Content":
        this.valueMarketingContent = parseInt(event.target.value) || 0;
        break;
      case "SMM":
        this.valueMarketingSMM = parseInt(event.target.value) || 0;
        break;
      case "GMB":
        this.valueMarketingGMB = parseInt(event.target.value) || 0;
        break;
      case "GoogleAbs":
        this.valueMarketingGoogleAbs = parseInt(event.target.value) || 0;
        break;
      case "GoogleAnalytics":
        this.valueMarketingGoogleAnalytics = parseInt(event.target.value) || 0;
        break;
      case "Others":
        this.valueMarketingOthers = parseInt(event.target.value) || 0;
        break;
    }
    this.allMarketingAdd = (this.valueMarketingSEO + this.valueMarketingSMO + this.valueMarketingContent + this.valueMarketingSMM + this.valueMarketingGMB + this.valueMarketingGoogleAbs + this.valueMarketingGoogleAnalytics + this.valueMarketingOthers);
    this.totalHours = this.allMobileAdd + this.allfrontAdd + this.allBackAdd + this.allTestAdd + this.allDesignAdd + this.allEcommerceAdd + this.allMarketingAdd;
    this.salesCallerCRMForm.patchValue({
      Estimate: this.totalHours,
    })

  }
  patchEstimateFun() {
    this.totalHours = this.allMobileAdd + this.allfrontAdd + this.allBackAdd + this.allTestAdd + this.allDesignAdd + this.allEcommerceAdd + this.allMarketingAdd;
    this.salesCallerCRMForm.patchValue({
      Estimate: this.totalHours,
    })
  }

  f(form: any) {
    if (this.salesCallerCRMForm.get(form)?.value) {
      return false
    } else return true
  }
  save() {
    const teleData = this.createForm(this.indexData);
    this.httpService.saveMarketingTeleData(teleData).subscribe(res => {
      if (res) {
        this.toasterService.success("lead successfully Send");
        this.salesCallerCRMForm.reset();
        this.changeStatus(this.status);
      }
    })
    this.httpService.uploadAttachment(this.attachmentData).subscribe(res => {
    })
  }

  mobileNO(event: any) {
    this.httpService.mobileNumber(event);
  }

  staffLeadData() {
    this.loader = true;
    this.httpService.findStaffLeadData(this.id, this.currentPage, 50).subscribe((res: any) => {
      console.log('this.id', this.id);

      console.log('res', res);

      this.staffData = res.result;
      this.loader = false;
      this.totalRecordsofFollowUp = res.totalRecords;
      for (let i = 0; i < this.staffData.length; i++) {
        this.staffData[i]['attachmentUrl'] = `../../assets/server-images/${this.staffData[i].attachment}`;
        this.staffData[i]['pdfUrl'] = `../../assets/server-images/${this.staffData[i].attachment}`;
      }
    })
  }

  pending() {
    this.loader = true;
    this.httpService.pendingLeads(this.id, this.currentPage, 50).subscribe((res: any) => {
      this.staffData = res.result
      if (this.staffData.length == 0) {
        this.NotData = true;
      }
      console.log(res.result, "res.result");
      this.totalRecordsofPendings = res.totalRecords;
      this.loader = false;
    })

  }

  hotLeads() {
    this.loader = true;
    this.httpService.hotLeads(this.id, this.currentPage, 50).subscribe((res: any) => {
      this.staffData = res.result
      this.loader = false;
      for (let i = 0; i < this.staffData.length; i++) {
        this.staffData[i]['attachmentUrl'] = `../../assets/server-images/${this.staffData[i].attachment}`;
        this.staffData[i]['pdfUrl'] = `../../assets/server-images/${this.staffData[i].attachment}`;
      }
      this.totalRecordsofHot = res.totalRecords
    })
  }

  coldLeads() {
    this.loader = true;
    this.httpService.coldLeads(this.id, this.currentPage, 50).subscribe((res: any) => {
      this.staffData = res.result;
      this.loader = false;
      for (let i = 0; i < this.staffData.length; i++) {
        this.staffData[i]['attachmentUrl'] = `../../assets/server-images/${this.staffData[i].attachment}`;
        this.staffData[i]['pdfUrl'] = `../../assets/server-images/${this.staffData[i].attachment}`;
      }
      this.totalRecordsofCold = res.totalRecords
    })
  }

  warmLeads() {
    this.loader = true;
    this.httpService.warmLeads(this.id, this.currentPage, 50).subscribe((res: any) => {
      this.staffData = res.result;
      this.loader = false;
      for (let i = 0; i < this.staffData.length; i++) {
        this.staffData[i]['attachmentUrl'] = `../../assets/server-images/${this.staffData[i].attachment}`;
        this.staffData[i]['pdfUrl'] = `../../assets/server-images/${this.staffData[i].attachment}`;
      }
      this.totalRecordsofWarm = res.totalRecords
    })
  }

  lostLeads() {
    this.loader = true;
    this.httpService.lostLeads(this.id, this.currentPage, 50).subscribe((res: any) => {
      this.staffData = res.result;
      this.loader = false;
      this.totalRecordsOfLost = res.totalRecords
    })
  }

  invoiceLeads() {
    this.staffData = []
    this.httpService.invoiceLeads(this.id, this.currentPage, 50).subscribe((res: any) => {
      this.staffData = res.result
      this.totalRecordsOfInvoice = res.totalRecords
    })
  }
  convertedLeads() {
    this.staffData = []
    this.httpService.convertedLeads(this.id, this.currentPage, 50).subscribe((res: any) => {
      this.staffData = res.result
      this.totalRecordsOfconvertedLead = res.totalRecords
    })
  }

  changeStatus(saleManageStatus: any) {
    this.status = saleManageStatus;
    this.currentPage = 0;
    switch (saleManageStatus) {
      case 'followup':
        this.staffLeadData();
        this.saleManageStatus = saleManageStatus;
        console.log('saleManageStatus', this.saleManageStatus);

        this.statusButton = saleManageStatus;
        break;
      case 'pending':
        this.pending();
        this.saleManageStatus = saleManageStatus
        break;
      case 'HOT':
        this.hotLeads();
        this.statusButton = saleManageStatus
        break;
      case 'COLD':
        this.coldLeads();
        this.statusButton = saleManageStatus
        break;
      case 'WARM':
        this.warmLeads();
        this.statusButton = saleManageStatus
        break;
      case 'LOST':
        this.lostLeads();
        this.statusButton = saleManageStatus
        break;
      case 'invoice':
        this.invoiceLeads();
        // this.followUpButton = false;
        this.saleManageStatus = saleManageStatus
        break;
      case 'converted':
        this.convertedLeads();
        // this.followUpButton = false;
        this.saleManageStatus = saleManageStatus
        break;
    }
  }

  isRequiredField(): boolean {
    let data = !this.salesCallerCRMForm.get('salesteleLeadStatus')?.value || (this.salesCallerCRMForm.get('salesteleLeadStatus')?.value !== '')
    return data
  }

  loadNextPage(selectedPage: any) {

    this.currentPage = selectedPage;
    this.currentPage
    if (this.status === 'pending')
      this.httpService.pendingLeads(this.id, this.currentPage - 1, 50).subscribe((res: any) => {
        this.staffData = res.result;
        // this.totalRecordsofPendings = res.totalRecords
      })
    if (this.status === 'followup')
      this.httpService.findStaffLeadData(this.id, this.currentPage - 1, 50).subscribe((res: any) => {
        this.staffData = res.result
      })
    if (this.status === 'HOT')
      this.httpService.hotLeads(this.id, this.currentPage - 1, 50).subscribe((res: any) => {
        this.staffData = res.result
      })
    if (this.status === 'COLD')
      this.httpService.coldLeads(this.id, this.currentPage - 1, 50).subscribe((res: any) => {
        this.staffData = res.result
      })
    if (this.status === 'WARM')
      this.httpService.warmLeads(this.id, this.currentPage - 1, 50).subscribe((res: any) => {
        this.staffData = res.result
      })
    if (this.status === 'LOST')
      this.httpService.lostLeads(this.id, this.currentPage - 1, 50).subscribe((res: any) => {
        this.staffData = res.result
      })

    if (this.status === 'invoice')
      this.httpService.invoiceLeads(this.id, this.currentPage - 1, 50).subscribe((res: any) => {
        this.staffData = res.result
      })
    if (this.status === 'converted')
      this.httpService.convertedLeads(this.id, this.currentPage - 1, 50).subscribe((res: any) => {
        this.staffData = res.result
      })
  }
  getStaffValue(staffId: any) {
    this.id = staffId.target.value;
    this.router.navigate(['admin/staffui', staffId.target.value]);
    this.changeStatus('pending');
  }

}

