import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffUiComponent } from './staff-ui.component';

describe('StaffUiComponent', () => {
  let component: StaffUiComponent;
  let fixture: ComponentFixture<StaffUiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StaffUiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffUiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
