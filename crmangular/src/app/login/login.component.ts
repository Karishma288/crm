import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { HttpService } from '../http.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss', '../admin/table.scss']
})
export class LoginComponent implements OnInit {
  showLoder: boolean = true;
  passwordText: boolean = false;
  mobileText: boolean = false;
  loginForm = this.fb.group({
    contact: ['', Validators.required],
    password: ['', Validators.required],

  });
  error: string | undefined;
  constructor(private router: Router, private fb: FormBuilder, private httpService: HttpService, private toasterService: ToastrService,) { }

  ngOnInit() {
  }

  loginEvent() {
    let today = new Date();
    let time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
  }

  login() {
    this.showLoder = false;
    let data: any = {
      contact: this.loginForm.get('contact')?.value,
      Password: this.loginForm.get('password')?.value,
    }

    this.httpService.postloginUser('LogIn', data).subscribe(res => {
      const data: any = res;
      this.showLoder = true;
      let setData = data.user1?.id || data.user1?._id
      let datsa = localStorage.setItem('staffId', setData);
      if (data.user1?.Role[0] == 'ADMIN') {
        let a = localStorage.setItem('login', data.user1.Role[0])
        this.router.navigate(['/admin/stafflist'])
      }
      else {
        // this.router.navigate(['/admin/stafflogin'])
        if (data.user1?.status) {
          this.router.navigate(['/user/staffui'])
        }
        else {
          this.toasterService.success("Your Session Is Expire Please Contact Super Admin")
        }
      }
    }, error => {
      this.showLoder = true;
      switch (error.error.message) {
        case 'contact is not correct':
          this.mobileText = true;
          break;
        case 'Password not correct':
          this.passwordText = true;
          this.mobileText = false;
      }
    })
  }
}

