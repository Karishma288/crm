import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss', '../admin/table.scss']
})
export class SidebarComponent implements OnInit {
  showSiderBar:boolean =true
  constructor(private router:Router) { }

    ngOnInit() {
      const login:any =localStorage.getItem('login');
      if(login == null || login == undefined){
        this.router.navigate(['/'])
      }
  }
  logout(){
    localStorage.removeItem('login')
    
    this.router.navigate(['/'])
  }

}
