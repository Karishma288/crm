import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffleaddataComponent } from './staffleaddata.component';

describe('StaffleaddataComponent', () => {
  let component: StaffleaddataComponent;
  let fixture: ComponentFixture<StaffleaddataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StaffleaddataComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffleaddataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
