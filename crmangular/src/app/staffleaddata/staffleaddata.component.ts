import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, MaxValidator, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { CSVService } from '../csv.service';
import { HttpService } from '../http.service';

@Component({
  selector: 'app-staffleaddata',
  templateUrl: './staffleaddata.component.html',
  styleUrls: ['./staffleaddata.component.scss', '../admin/table.scss']
})
export class StaffleaddataComponent implements OnInit {
  modalRef: any;
  modalRef1: any;
  staff: any;
  currentPage = 0;
  totalRecords = 0;
  sizePerPage = 25;
  notFound: any;
  crossIcon: boolean = false;
  totalRecordsData: any;
  leads: any;
  paginatinShow: boolean = true;
  borderDanger: boolean = false;
  isExportingCSV = false;
  isImportingCSV1 = false;
  loader: boolean = true;
  staffsData: any;
  leadNumData = new FormControl();
  config = {
    keyboard: true,
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'price-break-up car-lead-crm modal-xl',
    id: 1
  };
  config1 = {
    keyboard: true,
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'price-break-up car-lead-crm modal-xl',
    id: 2
  };
  addLeadData: FormGroup;
  paticularLead: any;
  data: any = [];
  constructor(private router: Router, private httpService: HttpService, private modalService: BsModalService, private formBuilder: FormBuilder, private toasterService: ToastrService, private csvService: CSVService) {
    this.addLeadData = formBuilder.group({
      Name: ['', Validators.required],
      Phone: ['', Validators.required],
      ProjectDetails: ['', Validators.required],
      Location: [''],
      Email: [''],
      Status: ['']
    })
  }

  ngOnInit(): void {
    this.staffData()
    this.httpService.getstafflist('getUser').subscribe(res => {
      this.staffsData = res;
      console.log("this.staffs====>", this.staffsData);

      this.loader = false;
      // console.log(" this.staffs==========>", this.staffs);
    })
  }



  staffData() {
    this.crossIcon = false;
    this.loader = true;
    this.leadNumData.reset();
    this.httpService.totalUploadData(this.currentPage, 20).subscribe((res: any) => {
      this.staff = res.result
      this.loader = false;
      this.paginatinShow = true;
      this.notFound = false;
      this.totalRecordsData = res.totalRecords
      console.log("this.totalRecordsData====>", this.totalRecordsData);
    })
  }

  loadNextPage(selectedPage: any) {
    this.currentPage = selectedPage;
    console.log("this.currentPage===>", this.currentPage);

    // this.httpService.totalUploadData(this.currentPage - 1, 20).subscribe(res => {
    //   this.staff = res
    //   this.loader = false;
    // })
    this.loader = true
    this.httpService.totalUploadData(selectedPage - 1, 20).subscribe((res: any) => {
      this.staff = res.result
      console.log("page===>", this.staff);
      this.loader = false
    })
  }
  searchLeads() {
    this.loader = true;
    if (this.leadNumData.value != undefined && this.leadNumData.value != null && this.leadNumData.value != '' && this.leadNumData.value.length >= 4 && this.leadNumData.value.length <= 6) {
      this.leads = this.leadNumData.value
      this.httpService.findPaticularLead(this.leads).subscribe((res: any) => {
        if (res.result == null) {
          this.staff = [];
          this.notFound = true;
          this.paginatinShow = false;
          this.loader = false;
        }
        else {
          this.staff = [res.result];
          this.notFound = false;
          this.loader = false;
          this.paginatinShow = false;
        }
      })
    }
    else {
      this.leads = this.leadNumData.value
      this.httpService.findPaticularMobileNo(this.leads).subscribe((res: any) => {
        if (res.result.length == 0) {
          this.staff = [];
          this.paginatinShow = false;
          this.notFound = true;
          this.loader = false;
        }
        else {
          this.staff = res.result;
          this.loader = false;
          this.notFound = false;
          this.paginatinShow = false;
        }
      })
    }
  }
  mobileNO(event: any) {
    this.crossIcon = true
    this.httpService.mobileNumber(event);
  }

  getStaffValue(staffId: any) {
    // this.loader = true;
    // this.httpService.findStaffData(staffId.target.value).subscribe(res => {
    //   this.staff = res;
    //   this.loader = false;
    //   console.log(res, "res===<>");
    // })
    this.router.navigate(['admin/staffui/', staffId.target.value]);
  }



  openAttachemnt(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, this.config);
  }
  openActionBox(template: TemplateRef<any>, indexValue: any) {
    this.paticularLead = this.staff[indexValue]
    this.modalRef1 = this.modalService.show(template, this.config1);
  }
  allDataGet(valueData: any) {
    const data = {
      CxName: valueData.value.Name,
      Phonenumber: valueData.value.Phone,
      Email: valueData.value.Email,
      Location: valueData.value.Location,
      leadId: '',
      salesteleLeadStatus: valueData.value.Status,
      ProjectDetails: valueData.value.ProjectDetails,
      salesTeleId: '',
    }
    this.httpService.findPaticularMobileNo(data.Phonenumber).subscribe(res => {
      this.data = res
       console.log('this.data ',this.data );
       
      if (this.data.result.length == 0) {
        this.httpService.uploadParticulrData(data).subscribe(res => {
          console.log('=============sanjay=', res);

          this.toasterService.success('Lead Upload Successfully')
        })
        this.modalRef.hide()

        this.addLeadData.reset();
      }
      else{
        alert('This Mobile number is Already exists')
      }
    })

  }

  exportLeadCSV() {
    this.isExportingCSV = true;
    const csvData = [];
    const csvRow = {
      Name: '',
      Phone: '',
      Email: '',
      Location: '',
      ProjectDetails: '',
      status: '',

    };
    csvData.push(csvRow);
    this.csvService.downloadFile(csvData, 'SalesCsv', ['Name', 'Phone', 'Email', 'Location', 'ProjectDetails', 'Status']);
    this.isExportingCSV = false;
  }

  exportLeadCSV1() {
    const csvData: any = [];
    this.httpService.totalUploadData(this.currentPage, 20).subscribe((res: any) => {
      const staffArray = res.result
      staffArray.forEach((ele: any) => {
        const csvRow = {
          Name: ele.CxName,
          Phone: ele.Phonenumber,
          Email: ele.Email,
          Location: ele.Location,
          ProjectDetails: ele.ProjectDetails,
          status: ele.salesteleLeadStatus,
        };
        csvData.push(csvRow);
      });
      this.csvService.downloadFile(csvData, 'SalesCsv', ['Name', 'Phone', 'Email', 'Location', 'ProjectDetails', 'Status']);
      this.isExportingCSV = false;
    });

  }
  onCSVSelected(event: any) {
    const files = event.target.files
    if (files && files.length > 0) {
      this.importCSV(files[0]);
    }
  }

  importCSV(csvFile: any) {
    this.isImportingCSV1 = true;
    this.csvService.csv2Array(csvFile).then((data: any[]) => {
      this.httpService.uploadCsvData(data).subscribe((res) => {
        if (res)
          this.toasterService.success('upload')

        this.isImportingCSV1 = false;

      })

    })
  }
}
