export function getPackageDate(date: any) {
    if (date != null && date != undefined && date != '') {
        var today = new Date(date);
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0');
        var yyyy = today.getFullYear();
        return yyyy + '-' + mm + '-' + dd;
    } else {
        return null
    }
}