import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StaffUiComponent } from './staff-ui/staff-ui.component';

import { ModalModule } from 'ngx-bootstrap/modal';
import { NgSelectModule } from "@ng-select/ng-select";
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NgxPaginationModule } from 'ngx-pagination';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { AdminModule } from './admin/admin.module';
import { StaffleaddataComponent } from './staffleaddata/staffleaddata.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { ToastrModule } from 'ngx-toastr';
import { BsDatepickerConfig, BsDatepickerModule } from 'ngx-bootstrap/datepicker';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    StaffUiComponent,
    AdminPanelComponent,
    StaffleaddataComponent

  ],

  imports: [
    BrowserModule,
    AdminModule,
    AppRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule,
    BrowserAnimationsModule,
    ModalModule.forRoot(),
    NgSelectModule,
    BsDropdownModule,
    NgMultiSelectDropDownModule,
    NgxPaginationModule,
    PdfViewerModule,
    ToastrModule.forRoot(),
    BsDatepickerModule.forRoot(),


  ],
  providers: [BsDatepickerConfig],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
