import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserhaderComponent } from './userhader.component';

describe('UserhaderComponent', () => {
  let component: UserhaderComponent;
  let fixture: ComponentFixture<UserhaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserhaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserhaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
