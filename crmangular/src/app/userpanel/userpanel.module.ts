import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SamepageComponent } from './samepage/samepage.component';
import { UsersidebarComponent } from './usersidebar/usersidebar.component';
import { UserhaderComponent } from './userhader/userhader.component';
import { AdminRoutingModule } from './user-routing.module';



@NgModule({
  declarations: [
    SamepageComponent,
    UsersidebarComponent,
    UserhaderComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class UserpanelModule { }
