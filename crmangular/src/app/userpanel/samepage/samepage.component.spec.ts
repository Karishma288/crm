import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SamepageComponent } from './samepage.component';

describe('SamepageComponent', () => {
  let component: SamepageComponent;
  let fixture: ComponentFixture<SamepageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SamepageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SamepageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
