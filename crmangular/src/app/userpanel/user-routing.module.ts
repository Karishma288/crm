import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StaffUiComponent } from '../staff-ui/staff-ui.component';
import { StaffleaddataComponent } from '../staffleaddata/staffleaddata.component';
import { SamepageComponent } from './samepage/samepage.component';
const routes: Routes = [
    {
        path: '', component: SamepageComponent,
        children: [
            { path: 'staffui', component: StaffUiComponent },
            { path: 'stafflead', component: StaffleaddataComponent }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdminRoutingModule { }
