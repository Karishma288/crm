import { Injectable } from '@angular/core';
// import { AngularCsv } from 'angular7-csv';
import {AngularCsv} from '../../../crmangular/node_modules/angular7-csv/dist/Angular-csv'


@Injectable({providedIn: 'root'})
export class CSVService {
    constructor() {}
    
     public downloadFile(data:any, filename : string, fields?: string[]){
         new AngularCsv(data, filename, {headers: fields});
    }

    // public csv2Array(csvFile: File): Promise<any> {
    //   console.log('======',csvFile)
    //     //read file from input        
    //     let reader: FileReader = new FileReader();
    //     return new Promise(resolve => {
    //         reader.readAsText(csvFile);
    //         reader.onload = (e) => {
    //         const csv:  any = reader.result?.toString()        
    //         const lines=csv.split("\n");
    //         const result = [];
    //         var headers=lines[0].split(",");
    //                for(var i=1;i<lines.length;i++) {
    //                 var obj :any;
    //                 lines[i] = this.replaceAll(lines[i], "\"", "");
    //                 var currentline=lines[i].split(",");
    //                 for(var j=0;j<headers.length;j++){
    //                     headers[j] = headers[j].replace('\r', '');
    //                     obj[headers[j]] = currentline[j];
    //                 }
    //                 result.push(obj);
    //             }
    //         return resolve(result); //JSON
    //         }
    //     });
    // }

    // replaceAll(string: string, search: string, replace: string) {
    //     return string.split(search).join(replace);
    // }

    public csv2Array(csvFile: File): Promise<any> {
      
      //read file from input        
      let reader: FileReader = new FileReader();
      return new Promise(resolve => {
          reader.readAsText(csvFile);        
          reader.onload = (e) => {
          const csv: any = reader.result?.toString();        
          const lines=csv.split("\n");          
          const result = [];
          var headers=lines[0].split(",");          
                 for(var i=1;i<lines.length;i++) {
                  var obj :any = {};
                  lines[i] = this.replaceAll(lines[i], "\"", "");
                  var currentline=lines[i].split(",");
                  for(var j=0;j<headers.length;j++){
                      headers[j] = headers[j].replace('\r', '');
                      obj[headers[j]] = currentline[j];
                  }
                  result.push(obj);
              }
          return resolve(result); //JSON
          }
      });
  }

  replaceAll(string:any, search:any, replace:any) {
      return string.split(search).join(replace);
  }


}