import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
@Injectable({
  providedIn: "root"
})
export class HttpService {
  test = "How r u?";
  port = ''
  private REST_API_SERVER = window.location.hostname + ':3000';

  constructor(private httpClient: HttpClient) { }

  getBaseUrl() {
    // return this.REST_API_SERVER;

    if (environment.production) {
      console.log('if', environment.production)
      return 'http://18.211.144.193:3000'
    } else {
      // console.log('else',environment.production)

      return 'http://localhost:3000'
    }

  }

  // mobileNumber() {

  // }
  mobileNumber(event: any) {
    var charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode < 48 || charCode > 57)) {
      event.preventDefault();
      return false;
    } else {
      return true;

    }
  }
  postnewstaff(url: any, data: any) {
    return this.httpClient.post(`${this.getBaseUrl()}/${url}`, data);
  }


  postsignUser(url: any, data: any) {
    return this.httpClient.post(`${this.getBaseUrl()}/${url}`, data);
  }
  postloginUser(url: any, data: any) {
    return this.httpClient.post(`${this.getBaseUrl()}/${url}`, data).pipe(
      catchError(this.handleError)
    );
  }
  handleError(error: any) {
    return throwError(error);
  }
  getstafflist(url: any) {
    return this.httpClient.get(`${this.getBaseUrl()}/${url}`);
  }
  putupdateStaff(id: any, data: any) {
    console.log("id==>", id);
    return this.httpClient.put(`${this.getBaseUrl()}/updateData/${id}`, data);
  }

  public uploadEmails(data: any) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.httpClient.post<any>('/api/uploadEmails', data);
  }

  getstaffData(data: any) {
    return this.httpClient.get(`${this.getBaseUrl()}/getstaffData/${data.id}`);

  }
  getStaff(data: any) {
    return this.httpClient.get(`${this.getBaseUrl()}/getStaff/${data.id}`);
  }


  public uploadCsvData(data: any) {
    console.log("innder leadl===>", data);
    return this.httpClient.post(`${this.getBaseUrl()}/uploadCsvData/csv`, data);
  }

  public findStaffLeadData(id: any, page: any, size: any) {
    return this.httpClient.get(`${this.getBaseUrl()}/findStaffLeadData/staffData/${id}/${page}/${size}`)
  }

  public saveMarketingTeleData(data: any) {
    return this.httpClient.put(`${this.getBaseUrl()}/saveMarketingTeleData/data`, data);
  }

  public pendingLeads(id: any, page: any, size: any) {
    size = 50;
    return this.httpClient.get(`${this.getBaseUrl()}/pendingLeads/data/${id}/${page}/${size}`)

  }
  public hotLeads(id: any, page: any, size: any) {
    size = 50;
    return this.httpClient.get(`${this.getBaseUrl()}/hotLeads/data/${id}/${page}/${size}`)

  }

  public coldLeads(id: any, page: any, size: any) {
    size = 50;
    return this.httpClient.get(`${this.getBaseUrl()}/coldLeads/data/${id}/${page}/${size}`)

  }

  public warmLeads(id: any, page: any, size: any) {
    size = 50;
    return this.httpClient.get(`${this.getBaseUrl()}/warmLeads/data/${id}/${page}/${size}`)

  }

  public lostLeads(id: any, page: any, size: any) {
    size = 50;
    return this.httpClient.get(`${this.getBaseUrl()}/lostLeads/data/${id}/${page}/${size}`)

  }

  public invoiceLeads(id: any, page: any, size: any) {
    size = 50;
    return this.httpClient.get(`${this.getBaseUrl()}/invoiceLeads/data/${id}/${page}/${size}`)
  }

  public convertedLeads(id: any, page: any, size: any) {
    size = 20;
    return this.httpClient.get(`${this.getBaseUrl()}/convertedLeads/data/${id}/${page}/${size}`)

  }

  public totalUploadData(page: any, size: any) {
    size = 20;
    return this.httpClient.get(`${this.getBaseUrl()}/totalUploadData/data/${page}/${size}`)

  }
  public findPaticularLead(id: any) {
    return this.httpClient.get(`${this.getBaseUrl()}/findPaticularLead/data/searchlead/${id}`);
  }
  public findPaticularMobileNo(id: any) {
    return this.httpClient.get(`${this.getBaseUrl()}/findPaticularMobileNo/data/searchmobile/${id}`);
  }
  
  public findPaticularBdeMobileNo(id: any,mobileNumber:any) {
    return this.httpClient.get(`${this.getBaseUrl()}/findPaticularBdeMobileNo/data/searchmobile/${id}/${mobileNumber}`);
  }
  
  public searchByDateRange(startDate:any,endDate:any,id: any,status:any) {
    return this.httpClient.get(`${this.getBaseUrl()}/searchByDateRange/date/${startDate}/${endDate}/${id}/${status}`);
  }
  public uploadAttachment(file: File) {
    const formData = new FormData();
    formData.append('file', file);
    return this.httpClient.post(`${this.getBaseUrl()}/addAttachment/images`, formData);
  }

  public uploadParticulrData(data: any) {
    console.log("innder leadl===>", data);
    return this.httpClient.post(`${this.getBaseUrl()}/uploadParticulrData/Data`, data);
  }

  updateLeadData(url: any, id: any, data: any) {
    console.log("id===>", id, "data---->", data);
    return this.httpClient.put(`${this.getBaseUrl()}/${url}/${id}`, data);
  }
  updateOnlyStatus(id: any, data: any) {
    return this.httpClient.put(`${this.getBaseUrl()}/updateOnlyStatus/${id}`, data);
  }
  findStaffData(id: any) {
    console.log("id====<>", id.id);
    return this.httpClient.get(`${this.getBaseUrl()}/findStaffData/${id.id}`);
  }
}
