import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SidebarComponent } from '../sidebar/sidebar.component';
import { NavbarComponent } from '../navbar/navbar.component';
import { MainComponent } from '../main/main.component';
import { StaffComponent } from '../staff/staff.component';
import { StafflistComponent } from '../stafflist/stafflist.component';
import { StaffUiComponent } from '../staff-ui/staff-ui.component';
import { StaffleaddataComponent } from '../staffleaddata/staffleaddata.component';
const routes: Routes = [
  {
    
    path: 'admin', component: NavbarComponent,
    children: [
      { path: 'staff', component: StaffComponent },
      { path: 'staff/update/:id', component: StaffComponent },
      { path: 'stafflist', component: StafflistComponent },
      { path: 'stafflogin', component: StaffUiComponent },
      { path: 'staffLead', component: StaffleaddataComponent },
      { path: 'staffui/:id', component: StaffUiComponent },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
