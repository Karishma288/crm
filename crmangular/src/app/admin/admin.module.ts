import {  NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { AdminRoutingModule } from './admin-routing.module';
import { SidebarComponent } from '../sidebar/sidebar.component';
import { NavbarComponent } from '../navbar/navbar.component';
import { MainComponent } from '../main/main.component';
import { FormsModule,ReactiveFormsModule} from '@angular/forms';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { StaffComponent } from '../staff/staff.component';
import { StafflistComponent } from '../stafflist/stafflist.component';
@NgModule({
  declarations: [
    
    SidebarComponent,
    NavbarComponent,
    MainComponent,
    StaffComponent,
    StafflistComponent
    
    
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ToastrModule.forRoot(),
    
  ],
providers: [ToastrService]
})
export class AdminModule { }




