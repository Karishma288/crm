require('angular')
require('ng-download-csv')

var exampleApp = angular.module('exampleApp', ['ngDownloadCsv'])

exampleApp.controller('exampleController', ['$scope', 'DownloadCSV', function ($scope, DownloadCSV) {

  // downloads a csv with filename 'a-good-title.csv'
  DownloadCSV({
    url: 'http://example.com/api/resource.csv',
    filename: 'a-good-title'
  }).success(function (data, status, headers, config) {
    // on download success ...
  }).error(function (data, status, headers, config) {
    // on download error ...
  })
}])
