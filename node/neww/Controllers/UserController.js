const { UserValidate } = require('../Validation/Uservalidate')
const createError = require('http-errors')
const bcrypt = require('bcryptjs')
const req = require('express/lib/request')
const res = require('express/lib/response')
const User = require('../Models/Usermodel')
const Mongo = require('mongoose')
const StaffModel = require('../Models/staffmodel')
const { default: mongoose } = require('mongoose');

// ***multer work ****
const multer = require('multer');
const path = require('path');
const DIR = path.join(__dirname, '../../../crmangular/src/assets/server-images');

// storage
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, DIR);
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname)

    }
});
//   const fileFilter = (req, file, cb) => {
//     if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png' || file.mimetype === 'image/webp' || file.mimetype === 'image/jpg' || file.mimetype === 'application/pdf' || file.mimetype === 'video/mp4') {
//       cb(null, true);
//     } else {
//       cb(new Error('Incorrect file'), false);
//     }
//   }
const Upload = multer({ storage: storage });
//const{Token}= require('../token/jwt')

// for user

const addAttachment = async (req, res) => {
}





const SignUp = async (req, res) => {
    try {
        const user = await UserValidate.validateAsync(req.body, User)
        const existEmail = await User.findOne({ Email: user.Email })
        const existContact = await User.findOne({ Contact: user.Contact })
        if (existEmail) {
            res.status(200).json('email error')
        }
        else if (existContact) {
            res.status(200).json('contact error')
        }
        else {
            const newUser = await new User(req.body).save()
            res.status(200).json('Successfull')
        }
    }
    catch (error) {
        res.status(400).json('exist')
    }
}
const LogIn = async (req, res) => {
    try {
        const user1 = await User.findOne({ Contact: req.body.contact })
        if (!user1) {
            return res.status(503).send({ status: 1, message: "contact is not correct" });
        }
        const isMatch = await bcrypt.compare(req.body.Password, user1.Password)
        if (!isMatch) {
            // return res.status(200).send('Password not correct')
            return res.status(503).send({ status: 1, message: 'Password not correct' });
        }
        // if (!user1 || !isMatch) {

        // }
        // else {
        return res.status(200).send({ user1 })

    }

    catch (error) {
        res.status(503).send({ status: 1, message: "contact and password incorrect" });
    }
}
const getUser = async (req, res) => {
    try {
        //   const id = req.params.id;
        const result = await User.find({ Name: { $nin: ["ADMIN", "admin"] } });
        // const product = await Product.findOne({ _id: id });
        if (!result) {
            return res.status(200).json('data not exist')
        }
        return res.status(200).send(result)
    } catch (error) {
        res.status(503).send({ status: 1, message: "error" })
    }

}
const getstaffData = async (req, res) => {
    try {
        const userId = req.params.id
        const product = await User.findOne({ _id: userId });
        if (!product) {
            return res.status(200).json('data not exist')
        }
        return res.status(200).send(product)
    } catch (error) {
        res.status(503).send({ status: 1, message: "error" })
    }

}
const getStaff = async (req, res) => {
    try {
        const id = req.params.id;
        const result = await User.findOne({ _id: id });
        if (!result) {
            return res.status(200).json('data not exist')
        }
        return res.status(200).send(result)
    } catch (error) {
        res.status(503).send({ status: 1, message: "error" })
    }

}

const updateData = async (req, res) => {
    try {
        // const id = req.params.id
        const id = req.params.id
        const updates = await bcrypt.hash(req.body.Password, 10);
        // const staffData = {
        //     Name: req.body.Name,
        //     Email: req.body.Email,
        //     Contact: req.body.Contact,
        //     Designation: req.body.Designation,
        //     Designation: req.body.Designation,
        //     Password: updates,
        //     PlainPass:req.body.Password
        // }
        // console.log("staffData==>",staffData);
        // console.log("updates==>", updates);

        const options = { new: true }
        // const resuult = await User.findById(id, req.body, options);
        // console.log("result==>", resuult);
        const resuult = await User.findById(id);
        resuult.Name = req.body.Name;
        resuult.Email = req.body.Email;
        resuult.Contact = req.body.Contact;
        resuult.Designation = req.body.Designation;
        // resuult.status = req.body.status;
        resuult.Password = updates;
        resuult.PlainPass = req.body.PlainPass;
        const result = await User.findByIdAndUpdate(id, resuult);
        if (!result) {
            return res.status(400).send('not update')
        }
        return res.status(200).json("Successfull");
    }
    catch (error) {
        res.status(503).send({ status: 1, message: "error" })
    }
}
const updateOnlyStatus = async (req, res) => {
    try {
        const id = req.params.id
        const options = { new: true }
        const resultData = await User.findById(id);
        resultData.status = req.body.status;
        resultData.revoke = req.body.revoke;
        const result = await User.findByIdAndUpdate(id, resultData);
        if (!result) {
            return res.status(400).send('not update')
        }
        return res.status(200).json("Successfull");
    }
    catch (error) {
        res.status(503).send({ status: 1, message: "error" })
    }
}

const updateLeadData = async (req, res) => {
    try {
        const id = req.params.id
        // const options = { new: true }
        const result = await StaffModel.findById(id);
        result.CxName = req.body.Name;
        result.Phonenumber = req.body.Phone;
        result.Email = req.body.Email;
        const resultData = await StaffModel.findByIdAndUpdate(id, result);
        if (!result) {
            return res.status(400).send('not update')
        }
        return res.status(200).json("Successfull");
    }
    catch (error) {
        res.status(503).send({ status: 1, message: "error" })
    }
}
const findStaffData = async (req, res) => {
    try {
        const id = mongoose.Types.ObjectId(req.params.id);
        const result = await StaffModel.find({ salesTeleId: id });
        if (!result) {
            return res.status(400).send('Not Data')
        }
        return res.status(200).json(result);
    }
    catch (error) {
        res.status(503).send({ status: 1, message: "error" })

    }
}

const uploadCsvData = async (req, res) => {
    try {
        let data = req.body;
        // console.log('data',data);
        for (let i = 0; i < data.length; i++) {
            const data_ = await StaffModel.find({ Phonenumber: data[i].Phone });
            if (data_.length == 0) {
                console.log('if');
                var leadId = Math.floor(Math.random()) + 1001;
                const product = await StaffModel.findOne().sort({ createdAt: -1 });
                // console.log('product',product);
                // const staffData = await User.findOne({ Designation: { $nin: ['admin','ADMIN'] }, revoke: true }).sort({ count: 1 })
                const staffData = await User.findOne({ Designation: { $ne: 'ADMINSTAFF' }, revoke: true }).sort({ count: 1 })
                // console.log('staffData', staffData);
                if (product === null || product === undefined || product === '') {
                    if (data[i].Name != '') {
                        const row = {
                            CxName: data[i].Name,
                            Phonenumber: data[i].Phone,
                            Email: data[i].Email,
                            Location: data[i].Location,
                            leadId: leadId,
                            salesteleLeadStatus: 'pending',
                            ProjectDetails: data[i].ProjectDetails,
                            salesTeleId: staffData._id,
                            Status: true
                        }
                        const newUser = await new StaffModel(row).save()
                    }
                }
                else {
                    if (product && product.leadId) {
                        leadId = product.leadId + 1

                    }
                    if (staffData.count === undefined || staffData.count === null || staffData.count === '') {
                        product.salesTeleId = staffData._id || staffData.id
                        staffData.count = 1
                    }
                    else {
                        product.salesTeleId = staffData._id || staffData.id
                        staffData.count = staffData.count + 1
                    }
                    let staff_ = await User.findByIdAndUpdate(staffData._id, staffData);
                    //   let product_=  await StaffModel.findByIdAndUpdate(product._id, product)

                    if (data[i].Name != '') {
                        console.log('esle');
                        const row = {
                            CxName: data[i].Name,
                            Phonenumber: data[i].Phone,
                            Email: data[i].Email,
                            Location: data[i].Location,
                            leadId: leadId,
                            salesteleLeadStatus: 'pending',
                            ProjectDetails: data[i].ProjectDetails,
                            salesTeleId: staffData._id,
                            // Status: 'Pending'
                        }
                        const newUser = await new StaffModel(row).save()
                    }
                    if (i == data.length - 1) {
                        return res.status(200).json('Data Upload SuccesFuly')
                    }
                }
            }
        }

    }
    catch (error) {
    }
}
const uploadParticulrData = async (req, res) => {
    try {
        let data = req.body;
        if (data) {
            var leadId = await StaffModel.findOne().sort({ createdAt: -1 });
            const product = await StaffModel.findOne().sort({ createdAt: -1 });
            if (product === null || product === undefined || product === '') {
                if (data.CxName != '') {
                    const row = {
                        CxName: data.CxName,
                        Phonenumber: data.Phonenumber,
                        Email: data.Email,
                        Location: data.Location,
                        source: data.source,
                        leadId: leadId,
                        salesteleLeadStatus: 'pending',
                        ProjectDetails: data.ProjectDetails,
                        salesTeleId: mongoose.Types.ObjectId(data.id),
                        Status: true
                    }
                    const newUser = await new StaffModel(row).save();
                    return res.status(200).json('Data Upload SuccesFuly')
                }
            }
            else {
                if (product && product.leadId) {
                    leadId = product.leadId + 1
                }

                // if (staffData.count === undefined || staffData.count === null || staffData.count === '') {
                //     product.salesTeleId = staffData._id || staffData.id
                //     staffData.count = 1
                // }
                // else {
                //     product.salesTeleId = staffData._id || staffData.id
                //     staffData.count = staffData.count + 1

                // }
                // await User.findByIdAndUpdate(staffData._id, staffData)
                await StaffModel.findByIdAndUpdate(product._id, product)
                if (data.CxName != '') {
                    const row = {
                        CxName: data.CxName,
                        Phonenumber: data.Phonenumber,
                        Email: data.Email,
                        Location: data.Location,
                        // id:data.id,
                        source: data.source,
                        leadId: leadId,
                        // salesteleLeadStatus: 'pending',
                        salesteleLeadStatus: data.salesteleLeadStatus,
                        ProjectDetails: data.ProjectDetails,
                        salesTeleId: mongoose.Types.ObjectId(data.id),
                        // Status: 'Pending'
                    }
                    const newUser = await new StaffModel(row).save();
                    return res.status(200).json({ status: 200, message: 'Data Upload SuccesFuly' });

                }
            }

        }
    }
    catch (error) {
    }
}

const saveMarketingTeleData = async (req, res) => {
    try {
        let data = req.body;
        const teleData = await StaffModel.findById({ _id: data.id })
        teleData['_id'] = data.id
        teleData['followUpDate'] = data.followUpDate;
        teleData['followUpTime'] = data.followUpTime;
        teleData['salesteleLeadStatus'] = data.salesteleLeadStatus;
        teleData['CxName'] = data.Name;
        teleData['Phonenumber'] = data.Phone;
        teleData['Email'] = data.Email;
        teleData['multipleMobile'] = data.multiplePh;
        teleData['multipleEmail'] = data.multipleEmail;
        teleData['googleDoc'] = data.googleDoc;
        teleData['quotationStatus'] = data.quotationStatus;
        teleData['remarks'] = data.remarks;
        teleData['technology'] = data.technology;
        teleData['webfront'] = data.webfront;
        teleData['webback'] = data.webback;
        teleData['marketing'] = data.marketing;
        teleData['Testing'] = data.Testing;
        teleData['Design'] = data.Design;
        teleData['Ecommerce'] = data.Ecommerce;
        teleData['jobTitle'] = data.jobTitle;
        teleData['comments'] = data.comments;
        teleData['requirements'] = data.requirements;
        teleData['Estimate'] = data.Estimate;
        teleData['monthWise'] = data.monthWise;
        teleData['mobile'] = data.mobile;
        teleData['meetingTime'] = data.meetingTime;
        teleData['attachment'] = data.attachment;
        teleData['type'] = data.type;
        teleData['Flutter'] = data.Flutter;
        teleData['ReactNative'] = data.ReactNative;
        teleData['Kotlin'] = data.Kotlin;
        teleData['Swift'] = data.Swift;
        teleData['Ruby'] = data.Ruby;
        teleData['Lua'] = data.Lua;
        teleData['Dart'] = data.Dart;
        teleData['ActionScript'] = data.ActionScript;
        teleData['Rust'] = data.Rust;
        teleData['Angular'] = data.Angular;
        teleData['React'] = data.React;
        teleData['Vue'] = data.Vue;
        teleData['Html'] = data.Html;
        teleData['Css'] = data.Css;
        teleData['Javascript'] = data.Javascript;
        teleData['Jquery'] = data.Jquery;
        teleData['Nodejs'] = data.Nodejs;
        teleData['NestJs'] = data.NestJs;
        teleData['Python'] = data.Python;
        teleData['Php'] = data.Php;
        teleData['Java'] = data.Java;
        teleData['Perl'] = data.Perl;
        teleData['ManualTesting'] = data.ManualTesting;
        teleData['AutomatedTesting'] = data.AutomatedTesting;
        teleData['MobileDeviceTesting'] = data.MobileDeviceTesting;
        teleData['FunctionalTesting'] = data.FunctionalTesting;
        teleData['UsabilityTesting'] = data.UsabilityTesting;
        teleData['CompatibilityTesting'] = data.CompatibilityTesting;
        teleData['PerformanceAndLoadTesting'] = data.PerformanceAndLoadTesting;
        teleData['SecurityTesting'] = data.SecurityTesting;
        teleData['AdobeXD'] = data.AdobeXD;
        teleData['Photopea'] = data.Photopea;
        teleData['Photoshop'] = data.Photoshop;
        teleData['Figma'] = data.Figma;
        teleData['FrontEnd'] = data.FrontEnd;
        teleData['BackEnd'] = data.BackEnd;
        teleData['BackEnd'] = data.BackEnd;
        teleData['SEO'] = data.SEO;
        teleData['SMO'] = data.SMO;
        teleData['Content'] = data.Content;
        teleData['SMM'] = data.SMM;
        teleData['GMB'] = data.GMB;
        teleData['GoogleAbs'] = data.GoogleAbs;
        teleData['GoogleAnalytics'] = data.GoogleAnalytics;
        teleData['Others'] = data.Others;
        const newUser = await StaffModel.findByIdAndUpdate(data.id, teleData);
        res.status(200).json({ Status: 200, message: newUser })
    }
    catch (error) {
    }
}

const findStaffLeadData = async (req, res) => {
    try {
        let size = req.params.size
        let page = req.params.page
        // let saleTeleId = req.params.id
        let saleTeleId = mongoose.Types.ObjectId(req.params.id)

        const result = await StaffModel.find({ salesteleLeadStatus: { $ne: "pending" }, salesTeleId: saleTeleId }).limit(size).skip(page * size).sort({ updatedAt: -1 });
        const totalRecords = await StaffModel.countDocuments({ salesteleLeadStatus: { $ne: "pending" }, salesTeleId: saleTeleId })
        return res.status(200).send({ result, totalRecords })
    } catch (error) {
        res.status(503).send({ status: 1, message: "error" })
    }

}

const findPaticularLead = async (req, res) => {
    try {
        let saleTeleId = req.params.leadId;
        const result = await StaffModel.findOne({ leadId: saleTeleId })
        const totalRecords = await StaffModel.countDocuments({ leadId: saleTeleId })
        return res.status(200).send({ result, totalRecords })
    } catch (error) {
        res.status(503).send({ status: 1, message: "error" })
    }
}
const findPaticularMobileNo = async (req, res) => {
    try {
        let saleTeleNo = req.params.mobile;
        const result = await StaffModel.find({ Phonenumber: saleTeleNo })
        const totalRecords = await StaffModel.countDocuments({ mobile: { $in: [saleTeleNo] } })
        return res.status(200).send({ result, totalRecords })
    } catch (error) {
        res.status(503).send({ status: 1, message: "error" })
    }
}


const findPaticularBdeMobileNo = async (req, res) => {
    try {

        let saleTeleNo = req.params.mobileNumber;
        // let id= req.params.id
        let saleTeleId = mongoose.Types.ObjectId(req.params.id)

        const result = await StaffModel.find({ salesTeleId: saleTeleId, Phonenumber: saleTeleNo })
        console.log('result', result);
        const totalRecords = await StaffModel.countDocuments({ salesTeleId: saleTeleId, Phonenumber: { $in: [saleTeleNo] } })
        return res.status(200).send({ result, totalRecords })
    } catch (error) {
        res.status(503).send({ status: 1, message: "error" })
    }
}

const searchByDateRange = async (req, res) => {
    try {
        // console.log('req.params.', req.params);
        let startDate = req.params.startDate;
        let endDate = req.params.endDate;
        let status = req.status;
        const CaptureDate = new Date(startDate);
        CaptureDate.setHours(5, 30, 0, 0);
        const tCaptureDate = new Date(endDate);
        tCaptureDate.setDate(tCaptureDate.getDate());
        tCaptureDate.setHours(5, 30, 0, 0);   
        let saleTeleId = mongoose.Types.ObjectId(req.params.id)
        const result = await StaffModel.find({ followUpDate: { $gte: CaptureDate, $lte: tCaptureDate }, salesTeleId: saleTeleId })
        console.log('result', result);
        const totalRecords = await StaffModel.countDocuments({ followUpDate: { $gte: CaptureDate, $lte: tCaptureDate }, salesTeleId: saleTeleId })
        return res.status(200).send({ result, totalRecords })
    } catch (error) {
        console.log('error',error);
        res.status(503).send({ status: 1, message: "error" })
    }
}
const hotLeadsData = async (req, res,) => {
    try {
        let size = req.params.size
        let page = req.params.page
        let saleTeleId = mongoose.Types.ObjectId(req.params.id)

        const result = await StaffModel.find({ salesteleLeadStatus: 'HOT', salesTeleId: saleTeleId }).limit(size).skip(page * size).sort({ updatedAt: -1 });
        const totalRecords = await StaffModel.countDocuments({ salesteleLeadStatus: 'HOT', salesTeleId: saleTeleId })

        return res.status(200).send({ result, totalRecords })
    } catch (error) {
        res.status(503).send({ status: 1, message: "error" })
    }

}


const coldLeads = async (req, res,) => {
    try {
        let size = req.params.size
        let page = req.params.page
        let saleTeleId = mongoose.Types.ObjectId(req.params.id)
        const result = await StaffModel.find({ salesteleLeadStatus: 'COLD', salesTeleId: saleTeleId }).limit(size).skip(page * size).sort({ updatedAt: -1 });
        const totalRecords = await StaffModel.countDocuments({ salesteleLeadStatus: 'COLD', salesTeleId: saleTeleId })

        return res.status(200).send({ result, totalRecords })
    } catch (error) {
        res.status(503).send({ status: 1, message: "error" })
    }

}

const warmLeads = async (req, res,) => {
    try {
        let size = req.params.size
        let page = req.params.page
        let saleTeleId = mongoose.Types.ObjectId(req.params.id)
        const result = await StaffModel.find({ salesteleLeadStatus: 'WARM', salesTeleId: saleTeleId }).limit(size).skip(page * size).sort({ updatedAt: -1 });
        const totalRecords = await StaffModel.countDocuments({ salesteleLeadStatus: 'WARM', salesTeleId: saleTeleId })
        return res.status(200).send({ result, totalRecords })
    } catch (error) {
        res.status(503).send({ status: 1, message: "error" })
    }

}

const lostLeads = async (req, res,) => {
    try {
        let size = req.params.size
        let page = req.params.page
        let saleTeleId = mongoose.Types.ObjectId(req.params.id)
        const result = await StaffModel.find({ salesteleLeadStatus: 'LOST', salesTeleId: saleTeleId }).limit(size).skip(page * size).sort({ updatedAt: -1 });
        const totalRecords = await StaffModel.countDocuments({ salesteleLeadStatus: 'LOST', salesTeleId: saleTeleId })

        return res.status(200).send({ result, totalRecords })
    } catch (error) {
        res.status(503).send({ status: 1, message: "error" })
    }

}

const pendingLeads = async (req, res,) => {
    try {
        console.log('req',req.params);
        let size = req.params.size
        let page = req.params.page
        let saleTeleId = mongoose.Types.ObjectId(req.params.id)
        const result = await StaffModel.find({ salesteleLeadStatus: "pending", salesTeleId: saleTeleId }).limit(size).skip(page * size).sort({ updatedAt: -1 });
        const totalRecords = await StaffModel.countDocuments({ salesteleLeadStatus: 'pending', salesTeleId: saleTeleId })
        return res.status(200).send({ result, totalRecords })
    } catch (error) {
        res.status(503).send({ status: 1, message: "error" })
    }

}
const invoiceLeads = async (req, res,) => {
    try {
        let size = req.params.size
        let page = req.params.page
        let saleTeleId = mongoose.Types.ObjectId(req.params.id)
        const result = await StaffModel.find({ salesteleLeadStatus: "Invoice", salesTeleId: saleTeleId }).limit(size).skip(page * size);
        const totalRecords = await StaffModel.countDocuments({ salesteleLeadStatus: 'Invoice', salesTeleId: salesTeleId })
        return res.status(200).send({ result, totalRecords })
    } catch (error) {
        res.status(503).send({ status: 1, message: "error" })
    }

}
const convertedLeads = async (req, res,) => {
    try {
        let size = req.params.size
        let page = req.params.page
        let saleTeleId = mongoose.Types.ObjectId(req.params.id)
        const result = await StaffModel.find({ salesteleLeadStatus: "Converted", salesTeleId: salesTeleId }).limit(size).skip(page * size);
        const totalRecords = await StaffModel.countDocuments({ salesteleLeadStatus: 'Converted', salesTeleId: saleTeleId })
        return res.status(200).send({ result, totalRecords })
    } catch (error) {
        res.status(503).send({ status: 1, message: "error" })
    }

}


const totalUploadData = async (req, res,) => {
    try {
        let size = req.params.size
        let page = req.params.page
        const result = await StaffModel.find().limit(size).skip(page * size).sort({ createdAt: -1 });
        // console.log("resultresult====>", result);
        const totalRecords = await StaffModel.countDocuments();
        return res.status(200).send({ result, totalRecords })
    } catch (error) {
        res.status(503).send({ status: 1, message: "error" })
    }
}

module.exports = {
    SignUp,
    LogIn,
    getUser,
    getStaff,
    updateData,
    updateOnlyStatus,
    getstaffData,
    uploadCsvData,
    findStaffLeadData,
    saveMarketingTeleData,
    pendingLeads,
    hotLeadsData,
    coldLeads,
    warmLeads,
    lostLeads,
    invoiceLeads,
    convertedLeads,
    totalUploadData,
    findPaticularLead,
    findPaticularMobileNo,
    findPaticularBdeMobileNo,
    searchByDateRange,
    addAttachment,
    Upload,
    uploadParticulrData,
    updateLeadData,
    findStaffData

}



