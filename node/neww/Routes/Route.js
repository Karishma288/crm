const express = require("express")
const router = express.Router()
const UserController = require('../Controllers/UserController')

router.post('/SignUp', UserController.SignUp);
router.post('/LogIn', UserController.LogIn);

router.get('/getUser', UserController.getUser);
router.put('/updateData/:id', UserController.updateData);
router.put('/updateOnlyStatus/:id', UserController.updateOnlyStatus);
router.get('/getstaffData/:id', UserController.getstaffData)
router.post('/uploadCsvData/csv', UserController.uploadCsvData);
router.get('/findStaffLeadData/staffData/:id/:page/:size', UserController.findStaffLeadData);
router.put('/saveMarketingTeleData/data', UserController.saveMarketingTeleData);
router.get('/hotLeads/data/:id/:page/:size', UserController.hotLeadsData)
router.get('/pendingLeads/data/:id/:page/:size', UserController.pendingLeads)
router.get('/coldLeads/data/:id/:page/:size', UserController.coldLeads)
router.get('/warmLeads/data/:id/:page/:size', UserController.warmLeads)
router.get('/lostLeads/data/:id/:page/:size', UserController.lostLeads)
router.get('/getStaff/:id', UserController.getStaff);
router.get('/invoiceLeads/data/:id/:page/:size', UserController.invoiceLeads);
router.get('/convertedLeads/data/:id/:page/:size', UserController.convertedLeads);
router.get('/totalUploadData/data/:page/:size', UserController.totalUploadData);
router.get('/findPaticularLead/data/searchlead/:leadId', UserController.findPaticularLead)
router.get('/findPaticularMobileNo/data/searchmobile/:mobile', UserController.findPaticularMobileNo);
router.get('/findPaticularBdeMobileNo/data/searchmobile/:id/:mobileNumber', UserController.findPaticularBdeMobileNo);
router.get('/searchByDateRange/date/:startDate/:endDate/:id/:status', UserController.searchByDateRange);
// ***image work***
router.post('/addAttachment/images', UserController.Upload.single('file'), UserController.addAttachment)
router.post('/uploadParticulrData/Data', UserController.uploadParticulrData);
router.put('/updateLeadData/:id', UserController.updateLeadData);
router.get('/findStaffData/:id', UserController.findStaffData);



module.exports = router