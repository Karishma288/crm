const jwt = require('jsonwebtoken')
const {SECRETKEY}= process.env

const Token = async (user) => {
    try {
        const data = {
            _id: user._id,
            Email: user.Email,
            Contact: user.Contact
        }
        
        const token = await jwt.sign({data: data },SECRETKEY,{expiresIn:"2D"})
        return token
        

    } catch (error) {
        return error
    }
}
const verifyToken = async(token) => {
    try {
        var verify = jwt.verify(token)
        return verify;
    } 
    catch (error) {
        return error
    }
}

module.exports ={
    Token,
    verifyToken
}