const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcryptjs')

const UserSchema = new Schema({
  Name: {
    type: String,
    // required: true
  },

  Email: {
    type: String,
    // unique: true
  },
  Contact: {
    type: String,
    // required: true,
    // unique: true
  },
  Designation: {
    type: String,
    // required: true
  },
  Password: {
    type: String,
    // required: true

  },
  PlainPass: {
    type: String,
    // required: true

  },
  Role:
  {
    type: Array,
    // required: true
  
  },

  count:
  {
    type:Number,
    // required:true
  },
  status:{
    type:Boolean,
    // required:true,
    default:true
  },
  revoke:{
    type:Boolean,
    // required:true,
    default:true
  }

},
  {
    timestamps: true


  })



UserSchema.pre('save', async function (next) {
  try {
    const salt = await bcrypt.genSalt(10)
    const hash = await bcrypt.hash(this.Password, salt)
    this.Password = hash

    next()
  } catch (error) {
    next(error)
  }
})


const User = mongoose.model('User1', UserSchema);
module.exports = User;