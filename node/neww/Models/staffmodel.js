const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcryptjs');
const { string, any, number } = require('joi');
const StaffSchema = new Schema({
  CxName: {
    type: String,
    required: false
  },

  id: {
    type: Object,
    required: false
  },
  Phonenumber: {
    type: String,
    required: false
  },
  Email: {
    type: String,
    required: false
  },
  Location: {
    type: String,
    required: false
  },

  Comments: {
    type: String,
    required: false
  },
  Remarks: {
    type: String,
    required: false
  },

  leadId:
  {
    type: Number,
    required: false
  },
  source: {
    type: String,
    required: false
  },
  followUpDate:
  {
    type: Date,
    required: false
  },
  salesteleLeadStatus:
  {
    type: String,
    required: false
  },
  followUpTime:
  {
    type: String,
    required: false
  },
  technology:
  {
    type: Array,
    required: false
  },
  jobTitle: {
    type: String,
    required: false
  },
  webfront:
  {
    type: Array,
    required: false
  },
  mobile:
  {
    type: Array,
    required: false
  },
  monthWise:
  {
    type: String,
    required: false
  },
  webback:
  {
    type: Array,
    required: false
  },
  marketing:
  {
    type: Array,
    required: false
  },
  Testing: {
    type: Array,
    required: false
  },
  Design: {
    type: Array,
    required: false
  },
  Ecommerce: {
    type: Array,
    required: false
  },
  multipleMobile: {
    type: Array,
    required: false
  },
  multipleEmail: {
    type: Array,
    required: false
  },
  googleDoc: {
    type: Array,
    required: false
  },
  attachment:
  {
    type: String,
    required: false
  },
  type:
  {
    type: String,
    required: false
  },
  quotationStatus:
  {
    type: String,
    required: false
  },
  remarks:
  {
    type: String,
    required: false
  },
  comments:
  {
    type: String,
    required: false
  },
  requirements:
  {
    type: String,
    required: false
  },

  Estimate:
  {
    type: Number,
    required: false
  },

  meetingTime:
  {
    type: String,
    required: false
  },

  ProjectDetails:
  {
    type: String,
    required: false
  },

  salesTeleId:
  {
    type: Object,
    required: false
  },
  // ***new work****
  React: {
    type: Number,
    required: false
  },
  Flutter: {
    type: Number,
    required: false
  },
  ReactNative: {
    type: Number,
    required: false
  },
  Kotlin: {
    type: Number,
    required: false
  },
  Swift: {
    type: Number,
    required: false
  },
  Ruby: {
    type: Number,
    required: false
  },
  Rust: {
    type: Number,
    required: false
  },
  Lua: {
    type: Number,
    required: false
  },
  Dart: {
    type: Number,
    required: false
  },
  ActionScript: {
    type: Number,
    required: false
  },
  Angular: {
    type: Number,
    required: false
  },
  React: {
    type: Number,
    required: false
  },
  Vue: {
    type: Number,
    required: false
  },
  Html: {
    type: Number,
    required: false
  },
  Css: {
    type: Number,
    required: false
  },
  Javascript: {
    type: Number,
    required: false
  },
  Jquery: {
    type: Number,
    required: false
  },
  Nodejs: {
    type: Number,
    required: false
  },
  NestJs: {
    type: Number,
    required: false
  },
  Python: {
    type: Number,
    required: false
  },
  Php: {
    type: Number,
    required: false
  },
  Java: {
    type: Number,
    required: false
  },
  Perl: {
    type: Number,
    required: false
  },
  ManualTesting: {
    type: Number,
    required: false
  },
  AutomatedTesting: {
    type: Number,
    required: false
  },
  MobileDeviceTesting: {
    type: Number,
    required: false
  },
  FunctionalTesting: {
    type: Number,
    required: false
  },
  UsabilityTesting: {
    type: Number,
    required: false
  },
  CompatibilityTesting: {
    type: Number,
    required: false
  },
  PerformanceAndLoadTesting: {
    type: Number,
    required: false
  },
  SecurityTesting: {
    type: Number,
    required: false
  },
  AdobeXD: {
    type: Number,
    required: false
  },
  Photopea: {
    type: Number,
    required: false
  },
  Photoshop: {
    type: Number,
    required: false
  },
  Figma: {
    type: Number,
    required: false
  },
  FrontEnd: {
    type: Number,
    required: false
  },
  BackEnd: {
    type: Number,
    required: false
  },
  SEO: {
    type: Number,
    required: false
  },
  SMO: {
    type: Number,
    required: false
  },
  Content: {
    type: Number,
    required: false
  },
  SMM: {
    type: Number,
    required: false
  },
  GMB: {
    type: Number,
    required: false
  },
  GoogleAbs: {
    type: Number,
    required: false
  },
  GoogleAnalytics: {
    type: Number,
    required: false
  },
  Others: {
    type: Number,
    required: false
  },

},
  { timestamps: true }
)

const StaffModel = mongoose.model('StaffData', StaffSchema);
module.exports = StaffModel;
