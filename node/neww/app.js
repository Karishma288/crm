const express = require('express');
const createError = require('http-errors');
const dotenv = require('dotenv').config();
const Routes = require('./Routes/Route');
const db = require('./Database/db');
const cors = require('cors')


const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors({origin: "*"}))
app.use(Routes);


app.listen( process.env.PORT|| 3000, () => {
    console.log('Server is running on..... ',process.env.PORT)
    })