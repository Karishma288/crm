const User = require('../Models/Usermodel')
const jwtToken =  require('../token/jwt')

const VerifyToken = async (req, res, next) => {
    try {
            const token = req.headers['authorization']

            if(!token){
                throw Error('Token not found');
            }

            let userData = await User.findOne({ token: token })
            if(!userData){
                throw Error('Not Authorized');
            }

            let userToken = await  jwtToken.verifyToken(token)
            // console.log("User Token ", userToken)

            if (userToken == "TokenExpiredError: jwt expired") {
                throw Error('JWT Token Expired')
            }
                req.user = userData;
                next()
        } catch (error) {
            res.status(401).send({error: error.message})
        }
}

module.exports = {VerifyToken}