const joi =require('joi')

const UserValidate =joi.object({
    Name:joi.string().required(),
    Contact:joi.string().length(10).pattern(/[6-9]{1}[0-9]{9}/).required(),
    Email:joi.string().lowercase().email().required(),
    Designation:joi.string().required(),
    Password:joi.string().lowercase().min(5).required(),
    PlainPass:joi.string().lowercase().min(5),
    Role:joi.string(),
    count:joi.number()
})

module.exports={UserValidate}
